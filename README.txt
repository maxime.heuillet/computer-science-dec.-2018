1. Mettez vos propres identifiants dans AUTRES/connection, vous permettant d'accéder à PostGreSQL
2. Créez les tables à l'aide du fichier SQL/BDD_SQL

3. Si vous voulez jouer, prenez les requêtes iniitialement présentes dans SQL/INSERT_SQL
    Un compte administrateur a pour pseudo administrateur et mot de passe administrateur
    Un compte de simple joueur a pour pseudo joueurjoueur et mot de passe joueurjoueur

4. Il faut ensuite exécuter le main

5. Si vous voulez les tests, le jeu de données de test est dans SQL/INSERT_TEST
   Les codes de tests sont dans le fichier tests_unitaires à la racine
   Les objets métiers et les dao qui correspondent sont dans OBJET_METIER et DAO respectivement