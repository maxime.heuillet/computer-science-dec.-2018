DELETE FROM medecin ;
DELETE FROM patient;
DELETE FROM salle_en_fonction;
DELETE FROM salle_a_acheter;
DELETE FROM bilan ; 
DELETE FROM partie ; 
DELETE FROM maladie ; 
DELETE FROM specialite ; 
DELETE FROM utilisateur ;

INSERT INTO SPECIALITE(specialite) VALUES ('spe1');
INSERT INTO SPECIALITE(specialite) VALUES ('spe2');
INSERT INTO SPECIALITE(specialite) VALUES ('spe3');
INSERT INTO SPECIALITE(specialite) VALUES ('spe4');

ALTER SEQUENCE maladie_id_maladie_seq RESTART WITH 1;
INSERT INTO MALADIE(specialite, nom_mal,nb_jours) VALUES ('spe1','mal1',1);
INSERT INTO MALADIE(specialite, nom_mal,nb_jours) VALUES ('spe2','mal2',2);
INSERT INTO MALADIE(specialite, nom_mal,nb_jours) VALUES ('spe3','mal3',3);
INSERT INTO MALADIE(specialite, nom_mal,nb_jours) VALUES ('spe4','mal4',4);
INSERT INTO MALADIE( nom_mal,nb_jours) VALUES ('malg1',1);
INSERT INTO MALADIE( nom_mal,nb_jours) VALUES ('malg2',1);
INSERT INTO MALADIE( nom_mal,nb_jours) VALUES ('malg3',1);

INSERT INTO salle_a_acheter(id_salle_temp,nom_salle) VALUES (0,'salle de generaliste');
INSERT INTO salle_a_acheter(id_salle_temp, nom_salle) VALUES (1,'couloir');
INSERT INTO salle_a_acheter(id_salle_temp,nom_salle) VALUES (2,'Bureau des prises de RDV');
ALTER SEQUENCE salle_a_acheter_id_salle_temp_seq RESTART WITH 3;
INSERT INTO salle_a_acheter(specialite,nom_salle) VALUES ('spe1','salle1');
INSERT INTO salle_a_acheter(specialite,nom_salle) VALUES ('spe2','salle2');
INSERT INTO salle_a_acheter(specialite,nom_salle) VALUES ('spe3','salle3');
INSERT INTO salle_a_acheter(specialite,nom_salle) VALUES ('spe4','salle4');
insert into utilisateur(pseudo) values ('testeur');
insert into partie(id_partie,pseudo,budget_initial,somme_a_atteindre,budget_actualise) values(0,'testeur',5000,5500,5000);
insert into bilan(id_bilan,pseudo,parties_gagnees,parties_perdues,parties_en_cours) values (3,'testeur',0,0,0);

ALTER SEQUENCE medecin_id_medecin_seq RESTART WITH 1;
ALTER SEQUENCE patient_id_patient_seq RESTART WITH 1;
