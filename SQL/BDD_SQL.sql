
create table UTILISATEUR (
	pseudo varchar(30) CONSTRAINT pk_utilisateur PRIMARY KEY,
	statut varchar(14),
	mot_de_passe varchar(30));

create table SPECIALITE (
	specialite varchar(50) CONSTRAINT pk_specialite PRIMARY KEY);

create table MALADIE (
	id_maladie serial CONSTRAINT pk_maladie PRIMARY KEY,
	specialite varchar(50) REFERENCES SPECIALITE,
	nom_mal varchar(50),
	nb_jours integer);

create table PARTIE (
	id_partie serial CONSTRAINT pk_partie PRIMARY KEY,
	pseudo varchar(30) REFERENCES UTILISATEUR,
	budget_initial integer,
	somme_a_atteindre integer,
	budget_actualise integer,
	compteur_medecins integer,
	avancement varchar(2));

create table BILAN (
	id_bilan serial CONSTRAINT pk_bilan PRIMARY KEY,
	pseudo varchar(30) REFERENCES UTILISATEUR,
	parties_gagnees integer,
	parties_perdues integer,
	parties_en_cours integer);

create table SALLE_A_ACHETER (
	id_salle_temp  serial CONSTRAINT pk_salle_a_acheter PRIMARY KEY,
	specialite varchar(50) REFERENCES SPECIALITE,
	nom_salle varchar(50));

create table SALLE_EN_FONCTION (
	id_salle serial CONSTRAINT pk_salle_en_fonction PRIMARY KEY,
	id_partie integer REFERENCES PARTIE,
	id_salle_temp integer REFERENCES SALLE_A_ACHETER,
	capacite_attente integer,
	prix integer);

create table PATIENT (
	id_patient  serial CONSTRAINT pk_patient PRIMARY KEY,
	id_maladie integer REFERENCES MALADIE, 
	id_partie integer REFERENCES PARTIE,
	id_salle integer REFERENCES SALLE_EN_FONCTION,
	nom_patient  varchar(30),
	prenom_patient  varchar(30),
	titre varchar(8),
	budget_patient integer,
	frais_avocat integer,
	jours_restants integer);


create table MEDECIN (

      id_medecin serial CONSTRAINT pk_medecin PRIMARY KEY,
	  nom_med varchar(30),
      prenom_med varchar(30),
	  specialite varchar(50) REFERENCES SPECIALITE,
      
      prix_med integer,
	  cout_consultation integer,
      capacite_traitement integer,
	
	  
      id_salle integer REFERENCES SALLE_EN_FONCTION,
      id_partie integer REFERENCES PARTIE);

