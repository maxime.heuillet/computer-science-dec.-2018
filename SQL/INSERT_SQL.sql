DELETE FROM medecin ;
DELETE FROM patient;
DELETE FROM salle_en_fonction;
DELETE FROM salle_a_acheter;
DELETE FROM bilan ; 
DELETE FROM partie ; 
DELETE FROM maladie ; 
DELETE FROM specialite ; 
DELETE FROM utilisateur ;

INSERT INTO SPECIALITE(specialite) VALUES ('radiologie');
INSERT INTO SPECIALITE(specialite) VALUES ('cardiologie');
INSERT INTO SPECIALITE(specialite) VALUES ('ophtalmologie');
INSERT INTO SPECIALITE(specialite) VALUES ('cancerologie');

ALTER SEQUENCE maladie_id_maladie_seq RESTART WITH 1;
INSERT INTO MALADIE(specialite, nom_mal,nb_jours) VALUES ('radiologie','os casse',5);
INSERT INTO MALADIE(specialite, nom_mal,nb_jours) VALUES ('cardiologie','crise cardiaque',1);
INSERT INTO MALADIE(specialite, nom_mal,nb_jours) VALUES ('ophtalmologie','myopie',7);
INSERT INTO MALADIE(specialite, nom_mal,nb_jours) VALUES ('cancerologie','cancer du poumon',3);
INSERT INTO MALADIE( nom_mal,nb_jours) VALUES ('rhume',8);
INSERT INTO MALADIE( nom_mal,nb_jours) VALUES ('faux certificat',10);
INSERT INTO MALADIE( nom_mal,nb_jours) VALUES ('saignement du nez',6);

INSERT INTO salle_a_acheter(id_salle_temp,nom_salle) VALUES (0,'salle de generaliste');
INSERT INTO salle_a_acheter(id_salle_temp, nom_salle) VALUES (1,'couloir');
INSERT INTO salle_a_acheter(id_salle_temp,nom_salle) VALUES (2,'Bureau des prises de RDV');
ALTER SEQUENCE salle_a_acheter_id_salle_temp_seq RESTART WITH 3;
INSERT INTO salle_a_acheter(specialite,nom_salle) VALUES ('radiologie','salle de radiologie');
INSERT INTO salle_a_acheter(specialite,nom_salle) VALUES ('cardiologie','salle de cardiologie');
INSERT INTO salle_a_acheter(specialite,nom_salle) VALUES ('ophtalmologie','salle de ophtalmologie');
INSERT INTO salle_a_acheter(specialite,nom_salle) VALUES ('cancerologie','salle de cancerologie');



insert into utilisateur(pseudo,statut,mot_de_passe) values ('administrateur','A','administrateur');
insert into utilisateur(pseudo,statut,mot_de_passe) values ('joueurjoueur','J','joueurjoueur');


insert into bilan(id_bilan,pseudo,parties_gagnees,parties_perdues,parties_en_cours) values (1,'administrateur',0,0,0);
insert into bilan(id_bilan,pseudo,parties_gagnees,parties_perdues,parties_en_cours) values (2,'joueurjoueur',0,0,0);
ALTER SEQUENCE bilan_id_bilan_seq RESTART WITH 3;