import psycopg2
from AUTRES.connection import connection

from OBJET_METIER.bilan import Bilan



class DAObilan:
    """
    Cette classe permet de faire le lien avec la table BILAN
    """

    def create(self, bilan):
        """
        input :

        * objet bilan qu'on veut faire entrer dans la table BILAN \\

        Cette fonction permet d'insérer le bilan dans la table \\

        output :
        
        * objet bilan (avec cette fois-ci son identifiant)
        """
        try:
            with connection.cursor() as cur:
                
                cur.execute("INSERT INTO BILAN (pseudo,parties_gagnees,parties_perdues,parties_en_cours) VALUES (%s, %s, %s, %s) RETURNING id_bilan;", (bilan.pseudo,bilan.parties_gagnees,bilan.parties_perdues,bilan.parties_en_cours))
                connection.commit()
                bilan.id_bilan = cur.fetchone()[0]

        except psycopg2.Error as error:
            connection.rollback()
            raise error
        finally:
            cur.close()
        return bilan

    def update(self, bilan):
        """
        input :
        
        * objet Bilan qui contient les nouvelles informations sur le bilan


        Cette fonction permet de mettre à jour dans la table BILAN l'objet Bilan
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "update BILAN set parties_gagnees=%s, parties_perdues=%s, parties_en_cours=%s where id_bilan=%s", (bilan.parties_gagnees, bilan.parties_perdues, bilan.parties_en_cours, bilan.id_bilan))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_nb_patients_couloir(self, id_partie):
        """
        input :

        * identifiant de la partie dont on veut récupérer le nombre de patients qui attendent dans le couloir \\

        Cette fonction permet de récupérer le nombre de patients qui attendent dans le couloir au cours d'une simulation dans une partie dont l'identifiant est placé en paramètre \\
        
        output :

        * nombre de patients qui attendent dans le couloir
        """
        with connection.cursor() as cur:
            try:
                cur.execute("select count(id_patient) from patient join salle_en_fonction using(id_salle) where id_salle_temp=1 and patient.id_partie=%s" % (id_partie))
                nb_patients_couloir = cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(nb_patients_couloir)
    
    def get_bilan(self, pseudo):
        """
        input :
        
        * pseudo du bilan qu'on veut récupérer

        Cette fonction permet de récupérer le bilan dont le pseudo est placé en paramètre \\

        output :
        
        * objet Bilan
        
        
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "select id_bilan, pseudo, parties_gagnees, parties_perdues, parties_en_cours from bilan where pseudo=%s", (pseudo,))

                found = cur.fetchone()
                if found:
                    return (Bilan(found[1], found[2], found[3], found[4], found[0]))

                return None
            except psycopg2.Error as error:
                connection.rollback()
                raise error




