from OBJET_METIER.salle_acheter import Salle_A_Acheter
from AUTRES.connection import connection

import psycopg2

# Table : SALLE_A_ACHETER
# Objets : Salle_achat
# on ajoute salle_achat

class DAOsalleacheter:
    """
    Cette classe permet de faire le lien avec la table SALLE_A_ACHETER
    """
    def create(self, salle):
        """
        input :

        * objet Salle_A_Acheter qu'on veut insérer dans la table SALLE_A_ACHETER \\

        Cette fonction permet d'insérer dans la table SALLE_A_ACHETER l'objet \\

        output :

        * objet Salle_A_Acheter (avec cette fois-ci son identifiant)
        """
        nom_add = str.lower(salle.nom_salle)
        with connection.cursor() as cur :
            M=[]
            cur.execute("SELECT nom_salle FROM SALLE_A_ACHETER")
            for row in cur:
                M.append(row)
        test=(str.lower(nom_add),)
        cur = connection.cursor()
        try:
            if test in M:
                print("La salle est déjà présente")
            else:
                cur.execute("INSERT INTO SALLE_A_ACHETER (specialite, nom_salle) VALUES (%s,%s) RETURNING id_salle_temp;", (salle.specialite,salle.nom_salle))
                salle.id_salle_temp = cur.fetchone()[0]
                # la transaction est enregistrée en base
                connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

        return salle


    def delete(self, salle):
        """
        input :

        * objet Salle_A_Acheter qu'on veut supprimer de la table SALLE_A_ACHETER \\

        Cette fonction permet de supprimer de la table SALLE_A_ACHETER la ligne correspondant à la salle placée en paramètre
        """
        with connection.cursor() as cur:
            try:
                cur.execute("delete from SALLE_A_ACHETER where id_salle_temp=%s" % (salle.id_salle_temp))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_all_salleacheter(self):
        """
        Cette fonction permet de récupérer les salles présentes dans SALLE_A_ACHETER \\

        output :

        * liste de salles (list of objets Salle_A_Acheter)
        """
        result=[]
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM SALLE_A_ACHETER" )
                found = cur.fetchone()
                salle= Salle_A_Acheter(found[1],found[2],found[0])
                
                result.append(salle)
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        salle = Salle_A_Acheter(found[1],found[2],found[0])
                        result.append(salle)
                return(result)
            except psycopg2.Error as error:
                connection.rollback()
                raise error


    def get_one_salleacheter(self,id_salle_temp):
        """
        input :

        * identifiant de la salle qu'on veut récupérer dans SALLE_A_ACHETER \\

        Cette fonction permet de récupérer une salle particulière dans SALLE_A_ACHETER \\

        output :

        * objet Salle_A_Acheter
        """
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM SALLE_A_ACHETER WHERE id_salle_temp=%s" % (id_salle_temp) )
                found = cur.fetchone()
                print(found)
                salle = Salle_A_Acheter(found[1],found[2],found[0])
                return(salle)
            except psycopg2.Error as error:
                connection.rollback()
                raise error
    
    def get_specialite_salle(self,id_salle_temp):
        """
        input :

        * identifiant de la salle dont on veut récupérer récupérer la spécialité, dans SALLE_A_ACHETER \\

        Cette fonction permet de récupérer la spécialité associée à une salle particulière dans SALLE_A_ACHETER \\

        output :
        
        * nom de la spécialité (string)
        """
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT specialite FROM SALLE_A_ACHETER WHERE id_salle_temp=%s"% (id_salle_temp) )
                found = cur.fetchone()
                
                return(found[0])
            except psycopg2.Error as error:
                connection.rollback()
                raise error