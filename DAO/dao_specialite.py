from OBJET_METIER.specialite import Specialite
from AUTRES.connection import connection

import psycopg2

# Table : SPECIALITE
# Objets : Specialite
# on ajoute spe

class DAOspecialite:
    """
    Cette classe permet de faire le lien avec la table SPECIALITE
    """
    def create(self,nom_spe):
        """
        input :

        * nom de la spécialité qu'on veut créer (string) \\

        Cette fonction permet d'insérer dans la table SPECIALITE la spécialité, si elle n'est pas encore présente
        """
        add_spe=Specialite(str.lower(nom_spe))

        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT * FROM SPECIALITE")
            for row in cur:
                M.append(row)
        test=(str.lower(add_spe.specialite),)

        cur = connection.cursor()
        try:
            if test in M:
                print('La spécialité est deja présente')
            else:
                cur.execute("INSERT INTO SPECIALITE(specialite) VALUES ('%s');" % (add_spe.specialite))
                # la transaction est enregistrée en base
                connection.commit()

        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()
            
    def generate_one(self):
        """
        Cette fonction permet de retourner une spécialité aléatoire parmi celles présentes dans la table SPECIALITE \\

        output :

        * spécialité sélectionnée
        """
        with connection.cursor() as cur:
            try:
                cur.execute('SELECT * FROM SPECIALITE ORDER BY RANDOM() LIMIT 1' )
                found=cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(found)

    
                
    def delete(self, special):
        """
        input :

        * nom de la spécialité qu'on veut supprimer \\

        Cette fonction permet de supprimer dans la table SPECIALITE la ligne correspondant à la spécialité dont le nom est placé en paramètre
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "delete from SPECIALITE where specialite=%s", (special,))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_all_specialites(self):
        """
        Cette fonction permet de récupérer les spécialités présentes dans SPECIALITE \\

        output :

        * liste des noms des spécialités présentes dans SPECIALITE
        """
        result=[]
        with connection.cursor() as cur:
            try:
                cur.execute("select * from specialite")
                spe=cur.fetchall()
                for i in spe:
                    result.append(i[0])
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(result)
    
    def get_all_specialites_objet(self):
        """
        Cette fonction permet de récupérer les spécialités présentes dans SPECIALITE \\

        output :
        
        * liste des spécialités présentes dans SPECIALITE (list of objects Specialite)
        """
        result=[]
        with connection.cursor() as cur:
            try:
                cur.execute("select * from specialite")
                spe=cur.fetchall()
                #print(spe)
                for i in spe:
                    result.append(Specialite(i[0]))
                #print(result)
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(result)

