from OBJET_METIER.salle_fonction import Salle_en_fonction
from AUTRES.connection import connection
from DAO.dao_medecin import DAOmedecin
import psycopg2

# Table : SALLE_EN_FONCTION
# Objets : Salle
# on ajoute salle


class DAOsallefonction:
    """
    Cette classe permet de faire le lien avec la table SALLE_EN_FONCTION
    """
    def create(self, salle):
        """
        input :

        * objet Salle_en_fonction qu'on veut insérer dans la table SALLE_EN_FONCTION \\

        Cette fonction permet d'insérer dans la table SALLE_EN_FONCTION l'objet \\

        output :

        * objet Salle_en_fonction (avec cette fois-ci son identifiant)
        """
        cur = connection.cursor()
        try:
            cur.execute(
                "INSERT INTO SALLE_EN_FONCTION ( id_partie, id_salle_temp, capacite_attente, prix) VALUES (%s, %s, %s, %s) RETURNING id_salle;", (salle.id_partie, salle.id_salle_temp, salle.capacite_attente, salle.prix))
            salle.id_salle = cur.fetchone()[0]
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

        return salle

    def update(self, salle):
        """
        input :

        * objet Salle_en_fonction contenant les nouvelles informations qu'on veut mettre à jour dans la table SALLE_EN_FONCTION \\

        Cette fonction permet de mettre à jour la ligne correspondant à la salle placée en paramètre, dans la table SALLE_EN_FONCTION
        """
        with connection.cursor() as cur:
            try:
                cur.execute("UPDATE SALLE_EN_FONCTION SET capacite_attente=%s, prix=%s  WHERE id_salle=%s" % (salle.capacite_attente, salle.prix, salle.id_salle) )
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
    
    def delete(self, id_salle):
        """
        input :

        * identifiant de la salle qu'on veut supprimer de la table SALLE_EN_FONCTION \\

        Cette fonction permet de supprimer de la table SALLE_EN_FONCTION la ligne correspondant à la salle dont l'identifiant est placé en paramètre
        """
        with connection.cursor() as cur:
            try:
                cur.execute("delete from SALLE_EN_FONCTION where id_salle=%s" % (id_salle))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_all_sallefonction_partie(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut récupérer les salles en fonctions \\

        Cette fonction permet de récupérer les salles en fonctions présentes dans une partie \\

        output :

        * liste de salles (list of objects Salle_en_fonction)
        """
        result=[]
        with connection.cursor() as cur:
            try:
                cur.execute("(select * from salle_en_fonction where id_partie=%s) except (select * from salle_en_fonction where ( (id_salle_temp=1 or id_salle_temp=2) and id_partie=%s) );"%(id_partie,id_partie) ) 
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                salle= Salle_en_fonction(found[1],found[2],found[3],found[4],found[0])
                #salle.__print__()
                result.append(salle)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        salle = Salle_en_fonction(found[1],found[2],found[3],found[4],found[0])
                        #patient.__print__()
                        result.append(salle)
            finally:
                return(result)        

    def get_one_sallefonction_partie(self,id_partie,id_salle):
        """
        input :

        * identifiant de la partie et identifiant de la salle en fonction qu'on veut récupérer \\

        Cette fonction permet de récupérer une salle en fonction particulière dans une partie \\

        output :

        * object Salle_en_fonction
        """
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM SALLE_EN_FONCTION where (id_partie=%s AND id_salle=%s)"%(id_partie,id_salle) ) # * -> id_salle, id_partie, id_salle_temp, capacite_attente, prix
                found = cur.fetchone()
                salle= Salle_en_fonction(found[1],found[2],found[3],found[4],found[0])
                
                
                return(salle)
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    
    def get_salles_generaliste(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut récupérer les salles en fonction généralistes \\

        Cette fonction permet de récupérer les salles en fonction généralistes dans une partie \\

        output :

        * liste de salles (list of objects Salle_en_fonction)
        """
        result=[]
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT id_salle FROM SALLE_EN_FONCTION WHERE id_partie=%s and id_salle_temp=0" % (id_partie) )
                found = cur.fetchall() 
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            except TypeError as error:
                pass
            else: 
                for i in found:
                    result.append(i[0])       
            finally:
                return(result)

    def salles_dispo(self,id_partie,specialite):
        """
        input :

        * identifiant de la partie en cours
        * spécialité à laquelle on s'interesse

        Permet de sélectionner les salles qui sont encore disponibles et qui ne sont pas attribuées, pour la spécialité demandée \\

        output :

        * liste des identifiants des salles encore disponibles à l'attribution (list)
        """
        result=[]
        if specialite is None:
            with connection.cursor() as cur:
                try:
                    cur.execute("(select id_salle from salle_en_fonction where id_salle_temp=0 and id_partie=%s) except (  (select id_salle from medecin where specialite is NULL and id_partie=%s) union (select id_salle from medecin join salle_en_fonction using(id_salle) where specialite is not NULL and id_salle_temp=0 and medecin.id_partie=%s) );" % (id_partie,id_partie,id_partie) )
                    found = cur.fetchall()
                except psycopg2.Error as error:
                    connection.rollback()
                    raise error
                except TypeError as error:
                    pass
                else:
                    for i in found:
                        result.append(i[0])
                finally:
                    return(result)        

        else:
            with connection.cursor() as cur:
                try:
                    cur.execute("(select id_salle from salle_en_fonction join salle_a_acheter using(id_salle_temp) where id_salle>1 and specialite='%s' and id_partie=%s) except (select id_salle from medecin where specialite='%s' and id_partie=%s)" % (specialite,id_partie,specialite,id_partie) )
                    found = cur.fetchall()
                except psycopg2.Error as error:
                    connection.rollback()
                    raise error
                except TypeError as error:
                    pass
                else:
                    for i in found:
                        result.append(i[0])
                finally:
                    return(result)

    def compte_salle_fonction_partie(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut connaitre le nombre de salles en fonction \\

        Cette fonction retourne le nombre de salles en fonction  (hormis le couloir et les RDV) dans une partie dont l'identifiant est placé en paramètre \\

        output :

        * nombre de salles en fonction (integer)
        """
        with connection.cursor() as cur:
            cur.execute(" SELECT COUNT(*) FROM SALLE_EN_FONCTION WHERE id_partie=%s" % (id_partie))
            compte = cur.fetchone()[0]
        return(compte-2)
    
    
    
    

    def occupation_stocks(self,id_partie,id_salle_temp):
        """
        input :

        * identifiant de la partie
        * identifiant de la salle à acheter à laquelle on s'interesse (en pratique 1 ou 2)

        Permet de connaitre l'occupation du couloir (1) ou de la salle des prises de RDV (2) \\

        output :

        * occupation de ces salles
        """
        with connection.cursor() as cur:
            cur.execute("select count(id_patient)-(select capacite_attente from salle_en_fonction where id_salle_temp=%s and id_partie=%s) from patient join salle_en_fonction using(id_salle) where (id_salle_temp=%s and patient.id_partie=%s and salle_en_fonction.id_partie=%s);" % (id_salle_temp,id_partie,id_salle_temp,id_partie,id_partie) )
            found=cur.fetchone()[0]
        return(found)

    def get_id_stocks(self,id_partie,id_salle_temp):
        """
        input :
        
        * identifiant partie
        * id_salle_temp (en pratique 1 ou 2)
        
        permet de connaitre l'id du couloir (1) ou de la salle des prises de rdv (2) pour une partie donnée \\

        output :
        
        * identifiants de ces salles

        """
        with connection.cursor() as cur:
            try:
                cur.execute('select id_salle from salle_en_fonction where id_salle_temp=%s and id_partie=%s'%(id_salle_temp,id_partie))
                found = cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            return(found)



    def capacite_generaliste(self,id_partie):
        """
        input :
        
        * identifiant de la partie

        permet de recuperer les salles de generaliste et leurs capacite d'attente \\

        output :
        
        * les salles de generaliste et leur capacite d'attente

        """
        with connection.cursor() as cur:
            cur.execute('SELECT id_salle,capacite_attente FROM MEDECIN JOIN SALLE_EN_FONCTION USING(id_salle) where salle_en_fonction.id_salle_temp=0 and medecin.id_partie=%s and salle_en_fonction.id_partie=%s' % (id_partie,id_partie))   
            found = cur.fetchall()
            connection.commit()
            return(found)
        
    def select_dispo(self,id_partie):
        """
        input:
        
        * identifiant de la partie à laquelle on s'interesse

        permet de selectionner les salles de generalistes qui n'ont pas encore ete attribuees a un medecin \\

        output :
        
        * les salles de generaliste qui n'ont pas encore ete attribuees

        """
        with connection.cursor() as cur:
            cur.execute('SELECT id_salle,capacite_attente from medecin JOIN salle_en_fonction USING(id_salle) where id_salle_temp=0 and medecin.id_partie=%s' % (id_partie))   
            found = cur.fetchall()
            if found is None:
                return(None)
            else : 
                return(found)
            connection.commit()
        

    def get_salles_partie(self, id_partie):
        """
        input:
        
        * identifiant de la partie

        permet d'obtenir l'ensemble des identifiants de salles et leur nom pour la partie \\

        output :
        
        * liste des salles avec leur identifiant et leur nom

        """
        with connection.cursor() as cur:
            cur.execute(" SELECT id_salle,nom_salle  FROM SALLE_EN_FONCTION join salle_a_acheter using(id_salle_temp) WHERE salle_en_fonction.id_partie=%s order by id_salle" % (id_partie)) 
            res=cur.fetchall()
            connection.commit()
            return(res)



