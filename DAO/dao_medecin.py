from OBJET_METIER.medecin import Medecin
from AUTRES.connection import connection

import psycopg2

class DAOmedecin:
    """
    Cette classe permet de faire le lien avec la table MEDECIN
    """
    def create(self, medecin):
        """
        input :
        
        * objet Medecin qu'on veut insérer dans la table MEDECIN \\

        Cette fonction permet d'insérer dans la table MEDECIN cet objet \\

        output :
        * objet Medecin (avec cette fois-ci son identifiant)
        """
        cur = connection.cursor()
        try:
            cur.execute("INSERT INTO MEDECIN (specialite, nom_med, prenom_med, prix_med, capacite_traitement, cout_consultation, id_salle,id_partie) VALUES ( %s, %s, %s, %s, %s, %s, %s, %s) RETURNING id_medecin;", (medecin.specialite, medecin.nom_med, medecin.prenom_med, medecin.prix_med, medecin.capacite_traitement, medecin.cout_consultation,medecin.id_salle,medecin.id_partie))
            medecin.id_medecin = cur.fetchone()[0]
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

        return medecin

    def update(self, medecin):
        """
        input :
        * objet Medecin \\

        Cette fonction permet de mettre le medecin à jour dans la table MEDECIN. Le medecin placé en paramètre contient les nouvelles informations
        """
        with connection.cursor() as cur:
            try:
                cur.execute("UPDATE MEDECIN SET id_salle=%s WHERE id_medecin=%s",  (medecin.id_salle, medecin.id_medecin) )
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def delete(self, medecin):
        """
        input :

        * objet medecin qu'on veut supprimer \\

        Cette fonction permet de supprimer dans la table MEDECIN la ligne correspondant au medecin placé en paramètre
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "delete from MEDECIN where id_medecin=%s" % (medecin.id_medecin))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    

    def get_one_medecin(self,id_medecin):
        """
        input :

        * identifiant du médecin qu'on veut récupérer \\

        Cette fonction permet de récupérer un objet médecin correspondant aux informations de la table MEDECIN relatives à l'identifiant placé en paramètre \\

        output :
        
        * objet Medecin
        """
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM MEDECIN WHERE id_medecin=%s" % (id_medecin) )
                found = cur.fetchone()
                med= Medecin(found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[0])
                return(med)
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_all_medecins_partie(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut récupérer les médecins \\

        Cette fonction permet de récupérer les médecins présents dans une partie dont l'identifiant est placé en paramètre \\

        output :
        
        * liste de médecins (list of objects Medecin)
        """
        doctors=[]
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM MEDECIN WHERE id_partie=%s order by specialite desc;" % (id_partie) )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

            try:
                medecin = Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                doctors.append(medecin)
            except TypeError as error:
                found=None 
            else: 
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        medecin = Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                        doctors.append(medecin)
            finally:
                return(doctors)

    def get_all_generaliste_partie(self,id_partie):
        """
        input :
        
        * indentifiant de la partie dont on veut récupérer les médecins généralistes \\

        Cette fonction permet de récupérer les médecins généralistes présents dans une partie dont l'identifiant est placé en paramètre \\

        output :
        
        * liste de médecins (list of objects Medecin)
        """
        doctors=[]
        with connection.cursor() as cur:
            try:
                cur.execute("select id_medecin, nom_med, prenom_med, specialite, prix_med, cout_consultation, capacite_traitement, id_salle, medecin.id_partie from medecin join salle_en_fonction using (id_salle) where (id_salle_temp=0 and medecin.id_partie=%s and salle_en_fonction.id_partie=%s) order by capacite_traitement desc;" % (id_partie,id_partie) )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                medecin= Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                doctors.append(medecin)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        medecin = Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                        #medecin.__print__()
                        doctors.append(medecin)
            finally:
                return(doctors)

    def get_all_specialiste_partie(self,id_partie):
        """
        input :

        * indentifiant de la partie dont on veut récupérer les médecins spécialistes \\

        Cette fonction permet de récupérer les médecins spécialistes présents dans une partie dont l'identifiant est placé en paramètre \\

        output :
        
        * liste de médecins (list of objects Medecin)
        """
        doctors=[]
        with connection.cursor() as cur:
            try:
                cur.execute("( SELECT * FROM MEDECIN WHERE (id_partie=%s and specialite is not NULL) ) except ( SELECT  id_medecin, nom_med, prenom_med, specialite, prix_med, cout_consultation, capacite_traitement, id_salle, medecin.id_partie from medecin join salle_en_fonction using(id_salle) where id_salle_temp=0 and medecin.id_partie=%s ) order by capacite_traitement desc;" % (id_partie,id_partie) )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                medecin= Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                #medecin.__print__()
                doctors.append(medecin)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        medecin = Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                        #medecin.__print__()
                        doctors.append(medecin)
            finally:
                return(doctors)

    def capacite_corps(self,id_partie,specialite):
        """
        input :

        * identifiant de la partie dont il est question
        * specialite pour laquelle on veut voir la capacité (string)

        Cette fonction donne la capacité de traitement du corps de spécialité correspondant \\

        output :
        * capacité de traitement du corps de spécialité précise (integer)
        """
        if specialite is None: 
            with connection.cursor() as cur:
                try:
                    cur.execute("SELECT sum(capacite_traitement) FROM MEDECIN WHERE (specialite is NULL and id_partie=%s) ;" % (id_partie) )
                    found = cur.fetchone()[0]
                except psycopg2.Error as error:
                    connection.rollback()
                    raise error
            return(found)
        else:
            with connection.cursor() as cur:
                try : 
                    cur.execute("SELECT sum(capacite_traitement) FROM MEDECIN WHERE (specialite='%s' and id_partie=%s) ;" % (specialite,id_partie) )
                    found = cur.fetchone()[0]
                except psycopg2.Error as error:
                    connection.rollback()
                    raise error
            return(found)

    def supprimer_affectations_partie(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut supprimer les affectations des médecins \\

        Cette fonction permet de supprimer toutes les affectations des médecins présents dans la partie : ils ne seront plus affectés à une salle particulière
        """
        liste_med = self.get_all_medecins_partie(id_partie)
        for med in liste_med :
            med.id_salle = None
            self.update(med)
            
    def get_all_generaliste_actif_partie(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut récupérer les généralistes actifs au cours d'une simulation \\

        Cette fonction permet de récupérer tous les médecins généralistes qui exercent lors d'une simulation
        """
        doctors=[]
        with connection.cursor() as cur:
            try:
                cur.execute("select id_medecin, nom_med, prenom_med, specialite, prix_med, cout_consultation, capacite_traitement, id_salle, medecin.id_partie from medecin join salle_en_fonction using (id_salle) where (id_salle_temp=0 and medecin.id_partie=%s and salle_en_fonction.id_partie=%s and id_salle is not NULL) order by capacite_traitement desc;" % (id_partie,id_partie) )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                medecin= Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                doctors.append(medecin)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        medecin = Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                        doctors.append(medecin)
            finally:
                return(doctors)

    def get_all_specialiste_actif_partie(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut récuperer les spécialistes actifs au cours d'une simulation \\

        Cette fonction permet de récupérer tous les médecins spécialistes qui exercent lors d'une simulation
        """
        doctors=[]
        with connection.cursor() as cur:
            try:
                cur.execute("( SELECT * FROM MEDECIN WHERE (id_partie=%s and specialite is not NULL and id_salle is not NULL) ) except ( SELECT  id_medecin, nom_med, prenom_med, specialite, prix_med, cout_consultation, capacite_traitement, id_salle, medecin.id_partie from medecin join salle_en_fonction using(id_salle) where id_salle_temp=0 and medecin.id_partie=%s ) order by capacite_traitement desc;" % (id_partie,id_partie) )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                medecin= Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                #medecin.__print__()
                doctors.append(medecin)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        medecin = Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                        #medecin.__print__()
                        doctors.append(medecin)
            finally:
                return(doctors)
    
    def compte_medecin_partie(self,id_partie):
        """
        input :
        
        * identifiant de la partie dont on veut connaitre le nombre de médecins \\

        Cette fonction renvoie le nombre de médecins présents dans une partie dont l'identifiant est placé en paramètre \\

        output :
        
        * nombre de médecins (integer)
        """
        with connection.cursor() as cur:
            cur.execute(" SELECT COUNT(*) FROM MEDECIN WHERE id_partie=%s" % (id_partie))
            compte = cur.fetchone()[0]
        return(compte)

