from OBJET_METIER.patient import Patient
from AUTRES.connection import connection
from DAO.dao_salle_fonction import DAOsallefonction
from DAO.dao_partie import DAOpartie

import psycopg2
import random

class DAOpatient:
    """
    Cette classe permet de faire le lien avec la table PATIENT
    """
    def create(self, patient):
        """
        input :

        * objet Patient qu'on veut insérer dans la table PATIENT \\

        Cette fonction permet d'insérer dans la table PATIENT le patient \\

        output :

        * objet Patient
        """
        try:
            with connection.cursor() as cur:
                
                cur.execute("INSERT INTO PATIENT (id_maladie, id_partie, id_salle, nom_patient, prenom_patient, titre, budget_patient, frais_avocat, jours_restants) VALUES (%s, %s, %s, %s, %s, %s,%s,%s,%s);", (patient.id_mal,patient.id_partie,patient.id_salle,patient.nom_patient,patient.prenom_patient,patient.titre,patient.budget_patient,patient.frais_avocat, patient.jours_restants))
                connection.commit()

        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

        return patient

    
    def update(self,patient):
        """
        input :

        * objet Patient qu'on veut mettre à jour dans la table PATIENT. Cet objet contient les nouvelles informations \\
        
        Cette fonction permet de mettre à jour le patient correspondant dans la table PATIENT
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                                                                                                                                                                    											
                    "update PATIENT set nom_patient=%s, prenom_patient=%s, titre=%s, budget_patient=%s, frais_avocat=%s, jours_restants=%s,id_maladie=%s,id_partie=%s,id_salle=%s where id_patient=%s", (patient.nom_patient, patient.prenom_patient, patient.titre, patient.budget_patient, patient.frais_avocat, patient.jours_restants,patient.id_mal,patient.id_partie,patient.id_salle, patient.id_patient))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def delete(self,id_patient):
        """
        input :

        * identifiant du patient qu'on veut supprimer de la table PATIENT \\

        Cette fonction permet de supprimer de la table PATIENT la ligne correspondant au patient dont l'identifiant est placé en paramètre
        """
        with connection.cursor() as cur:
            try:
                cur.execute("delete from PATIENT where id_patient=%s" % (id_patient))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def all_patients_to_attribute(self,id_partie):
        """
        input :

        * identifiant de la partie en cours

        Cette fonction permet d'obtenir la liste des patients auxquels il faut attribuer une salle \\

        output :

        * liste des patients à attribuer (list of objects Patient)
        """
        result=[]
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM PATIENT WHERE id_salle is NULL and id_partie=%s" % (id_partie) )
                found = cur.fetchone()
                patient= Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                #patient.__print__()
                result.append(patient)
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        patient = Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                        #patient.__print__()
                        result.append(patient)
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(result)

    def all_patients_salle(self,id_partie,id_salle):
        """
        input :

        * identifiant de la partie et identifiant de la salle dont on veut récupérer les patients \\

        Cette fonction permet de récupérer tous les patients présents dans une salle (dont l'identifiant est placé en paramètre) dans une partie (dont l'identifiant est placé en paramètre) \\
        
        output :

        * liste de patients (list of objects Patient)
        """
        journee=[]
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT id_patient,id_maladie,id_partie,id_salle,nom_patient,prenom_patient,titre,budget_patient,frais_avocat,jours_restants FROM PATIENT WHERE id_salle=%s and id_partie=%s" % (id_salle,id_partie) )
                found = cur.fetchone()
            
                patient=Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                journee.append(patient)

                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        patient = Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                        journee.append(patient)
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(journee)

    def all_patients_specialite_pour_specialistes(self,id_partie,specialite):
        """
        input :

        * identifiant de la partie
        * spécialité considérée (string)

        Cette fonction donne la liste des patients qui sont dans le couloir et qui ont besoin de consulter la spécialité placée en paramètre \\

        output :

        * liste de ces patients (list of objects Patient)
        """
        journee=[]
        with connection.cursor() as cur:
            try:
                cur.execute("select id_patient,id_maladie,id_partie,id_salle,nom_patient,prenom_patient,titre,budget_patient,frais_avocat,jours_restants from PATIENT join MALADIE using (id_maladie) where (id_salle=(select id_salle from salle_en_fonction where id_salle_temp=1 and id_partie=%s) and specialite='%s');" % (id_partie,specialite))
                found = cur.fetchone()
                if found is not None : 
                    patient = Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                    journee.append(patient)
                
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        patient = Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                        journee.append(patient)
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            return(journee)

    def information_patient(self,id_patient,id_partie):
        """
        input :

        * identifiant du patient et de la partie dont on veut récupérer la spécialité nécessaire pour le soigner \\

        Cette fonction permet de récupérer la spécialité nécessaire pour soigner le patient (dont l'identifiant est placé en paramètre), dans la partie (dont l'identifiant est placé en paramètre) \\

        output :

        * nom de la spécialité (string)
        """
        with connection.cursor() as cur:
            
            try:
                cur.execute("SELECT specialite FROM MALADIE WHERE id_maladie=(SELECT id_maladie FROM PATIENT WHERE id_patient=%s and id_partie=%s) ; " % (id_patient,id_partie) )
                nom_spe=cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(nom_spe)

    def composition_patient(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut obtenir les maladies présentes \\

        Cette fonction permet d'obtenir les maladies présentes chez les patients d'une partie et le nombre de patients qui l'ont \\

        output :

        * liste contenant les listes des identifiants des maladies et le nombre de patients l'ayant (list of list(identifiant, integer))
        """
        resultat=[]
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT id_maladie,count(id_maladie) FROM PATIENT WHERE id_partie=%s GROUP BY id_maladie ORDER BY id_maladie; " % (id_partie) )
                res=cur.fetchall()
                for i in res:
                    resultat.append([i[0], i[1] ] )
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(resultat)

    def besoin_specialiste(self,id_patient,id_partie):
        """
        input :

        * identifiant du patient et identifiant de la partie, qu'on veut mettre à jour \\

        Cette fonction permet d'attribuer au patient l'identifiant du couloir dans lequel il se trouve (différent suivant les parties)
        """
        with connection.cursor() as cur:
            try:
                cur.execute('UPDATE PATIENT SET id_salle=(select id_salle from salle_en_fonction where id_salle_temp=1 and id_partie=%s) where id_patient=%s' % (id_partie,id_patient) )
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def recupere_liste_maladies(self):
        """
        Cette fonction permet de récupérer la liste des identifiants des maladies présentes chez les patients \\

        output :

        * liste des identifiants (list)
        """
        resultat=[]
        with connection.cursor() as cur:
            try:
                cur.execute('select id_maladie from PATIENT' )
                res=cur.fetchall()
                for i in res:
                    resultat.append(i[0])
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(resultat)



    def get_all_patients_partie(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut récupérer les patients \\

        Cette fonction permet de récuperer les patients présents dans une partie dont l'identifiant est placé en paramètre \\

        output :

        * liste de patients (list of objects Patient)
        """
        result=[]
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM PATIENT WHERE id_partie=%s" % (id_partie) )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                patient= Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                #patient.__print__()
                result.append(patient)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        patient = Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                        #patient.__print__()
                        result.append(patient)
            finally:
                return(result)
            
    def get_all_patients(self):
        """
        Cette fonction permet de récuperer tous les patients présents dans PATIENT \\

        output :

        * liste de patients (list of objects Patient)
        """
        result=[]
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM PATIENT " )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                patient= Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                #patient.__print__()
                result.append(patient)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        patient = Patient(found[0],found[1],found[2],found[3],found[4],found[5],found[6],found[7],found[8],found[9])
                        #patient.__print__()
                        result.append(patient)
            finally:
                return(result)
        
    
    
    def compte_maladie_salle(self,id_partie,id_salle):
        """
        input :

        * identifiant de la partie
        * identifiant de la salle dont on veut savoir les maladies des patients présents dedans \\

        Cette fonction permet d'obtenir les identifiants des maladies des patients présents dans une salle donnée, et le nombre de patients qui l'ont \\

        output :

        * identifiants et nombre de patients (integer)
        """
        with connection.cursor() as cur:
            cur.execute("SELECT nom_mal,count(id_maladie) FROM patient join maladie using(id_maladie) WHERE id_partie=%s and id_salle=%s GROUP BY nom_mal ORDER BY nom_mal; " % (id_partie,id_salle) )
            res=cur.fetchall()
            connection.commit()
            return(res)
        
    def compte_maladie_salle_specialite(self,id_partie,id_salle,specialite):
        """
        input :

        * identifiant de la partie dont il est question
        * identifiant de la salle à laquelle on s'interesse
        * spécialité à laquelle on s'interesse

        Cette fonction compte le nombre de malades pour chaque maladie ayant besoin de la spécialité mentionnée dans les patients qui sont dans les salles de généralistes \\

        output :

        * liste des maladies avec leurs fréquences respectives
        """
        with connection.cursor() as cur:
            cur.execute("SELECT nom_mal,count(id_maladie) FROM patient JOIN maladie using(id_maladie) WHERE id_partie=%s and id_salle=(select id_salle from salle_en_fonction where id_salle_temp=1 and id_partie=%s) and specialite='%s' GROUP BY nom_mal ORDER BY nom_mal; " % (id_partie,id_partie,specialite) )
            res=cur.fetchall()
            connection.commit()
            return(res)

    def compter_patient_couloir(self,id_partie):
        """
        input :

        * identifiant de la partie dont on veut obtenir le nombre de patients dans le couloir \\

        Cette fonction permet d'obtenir le nombre de patients dans le couloir pour une partie donnée dont l'identifiant est placé en paramètre \\

        output :
        
        * nombre de patients dans le couloir (integer)
        """
        with connection.cursor() as cur:

            cur.execute("select count(id_patient) from patient where id_salle=(select id_salle from salle_en_fonction where id_salle_temp=1 and id_partie=%s) and id_partie=%s ;" % (id_partie,id_partie))
            found = cur.fetchone()[0]
            connection.commit()
            return(found)

    def compter_patient_salle(self,id_partie,id_salle):
        """
        input :

        * identifiant de la partie
        * identifiant de la salle dont on veut obtenir le nombre de patients qui sont dedans \\

        Cette fonction permet d'obtenir le nombre de patients dans une salle donnée pour une partie donnée \\

        output :

        * nombre de patients dans la salle (integer)
        """
        with connection.cursor() as cur:
            cur.execute(" SELECT COUNT(*) FROM PATIENT WHERE id_salle=%s and id_partie=%s" % (id_salle,id_partie))
            occupation = cur.fetchone()[0]
            connection.commit()
            return(occupation)

    def preparer_mort_generaliste(self,id_partie):
        """
        input :

        * identifiant de la partie en cours

        Cette fonction compte le nombre de patients qui sont dans l'attente d'un RDV \\

        output :

        * nombre de patients dans l'attente d'un RDV (integer)
        """
        with connection.cursor() as cur:
        #    cur.execute("select count(id_patient),sum(frais_avocat) from patient where id_salle=0 and id_partie=%s and jours_restants=0  ;" % (id_partie))
        #    found1 = cur.fetchall()
            
            cur.execute("select count(id_patient) from patient join salle_en_fonction using(id_salle) where id_salle_temp=2 and patient.id_partie=%s and salle_en_fonction.id_partie=%s" % (id_partie,id_partie))
            found2 = cur.fetchone()[0]
            connection.commit()
        return(found2)
    
    def preparer_mort_specialiste(self,id_partie):
        """
        input :

        * identifiant de la partie en cours

        Cette fonction compte le nombre de patients qui sont en attente dans le couloir \\

        output :

        * nombre de patients qui attendent dans le couloir (integer)
        """
        with connection.cursor() as cur:
        #    cur.execute("select count(id_patient),sum(frais_avocat) from patient where id_partie=%s and jours_restants=0 and id_salle=(select id_salle from salle_en_fonction where id_salle_temp=1 and id_partie=%s)  ;" % (id_partie,id_partie))
        #    found1 = cur.fetchall()
            
            cur.execute("select count(id_patient) from patient where id_salle=(select id_salle from salle_en_fonction where id_salle_temp=1 and id_partie=%s) and id_partie=%s ;" % (id_partie,id_partie))
            found2 = cur.fetchone()[0]
            connection.commit()
        return(found2)
    
    def recupere_info_couloir(self,id_partie):
        """
        input :

        * identifiant de la partie en cours

        Compte les patients du couloir, la capacité d'attente du couloir et précise l'effectif de chaque maladie dans le couloir par spécialité \\

        output :

        * vecteur avec en première position la fréquence, en seconde la capacité du couloir et en troisième le tableau des fréquences pour chaque maladie
        """
        with connection.cursor() as cur:
            found2 = self.compter_patient_couloir(id_partie)
            cur.execute("select count(id_patient) from patient where id_salle=(select id_salle from salle_en_fonction where id_salle_temp=1 and id_partie=%s) and id_partie=%s ;" % (id_partie,id_partie))
            found2 = cur.fetchone()[0]

            cur.execute("select capacite_attente from salle_en_fonction where id_salle_temp=1 and id_partie=%s ;" % (id_partie))
            found3 = cur.fetchone()[0]

            cur.execute("select id_maladie, nom_mal, specialite, count(id_patient) as repartition from patient join maladie using(id_maladie) where id_salle=(select id_salle from salle_en_fonction where id_salle_temp=1 and id_partie=%s) and id_partie=%s group by id_maladie, nom_mal, specialite order by repartition desc;" % (id_partie,id_partie))
            found4 = cur.fetchall()
            connection.commit()
        return(found2,found3,found4)

    
    def get_nb_patients_couloir(self, id_partie):
        """
        input :
        
        * identifiant de la partie dont on veut récupérer le nombre de patients qui attendent dans le couloir \\

        Cette fonction permet de récupérer le nombre de patients qui attendent dans le couloir au cours d'une simulation dans une partie dont l'identifiant est placé en paramètre \\

        output :
        
        * nombre de patients qui attendent dans le couloir
        """
        with connection.cursor() as cur:
            try:
                cur.execute("select count(id_patient) from patient join salle_en_fonction using(id_salle) where id_salle_temp=1 and patient.id_partie=%s" % (id_partie))
                nb_patients_couloir = cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(nb_patients_couloir)