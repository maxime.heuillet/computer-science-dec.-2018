from OBJET_METIER.utilisateur import Utilisateur
from AUTRES.connection import connection

import psycopg2

# Table : UTILISATEUR
# Objets : Utilisateur
# on ajoute utilisateur

class DAOutilisateur:
    """
    Cette classe permet de faire le lien avec la table UTILISATEUR
    """
    def create(self, utilisateur):
        """
        input :

        * objet Utilisateur qu'on veut insérer dans la table UTILISATEUR \\

        Cette fonction permet d'insérer dans UTILISATEUR une ligne correspondant à l'objet Utilisateur placé en paramètre \\

        output :

        * objet Utilisateur
        """
        cur = connection.cursor()
        try:
            cur.execute(
                "INSERT INTO UTILISATEUR (pseudo, statut, mot_de_passe) VALUES (%s, %s, %s)", (utilisateur.pseudo, utilisateur.statut, utilisateur.mot_de_passe))


            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

        return utilisateur

    def update(self, utilisateur):
        """
        input :

        * objet Utilisateur contenant de nouvelles informations, qu'on veut mettre à jour dans la table UTILISATEUR \\

        Cette fonction permet de mettre à jour la ligne correspondant à l'utilisateur dans UTILISATEUR
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "update UTILISATEUR set statut=%s, mot_de_passe=%s where pseudo=%s", (utilisateur.statut, utilisateur.mot_de_passe, utilisateur.pseudo))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def delete(self, utilisateur):
        """
        input :

        * objet Utilisateur qu'on veut supprimer de la table Utilisateur \\

        Cette fonction permet de supprimer la ligne correspondant à l'utilisateur placé en paramètre, dans la table UTILISATEUR
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "delete from UTILISATEUR where pseudo=%s", (utilisateur.pseudo,))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_all_utilisateurs(self):
        """
        Cette fonction permet de récupérer tous les joueurs et administrateurs \\

        output :

        * liste d'utilisateurs (list of objects Utilisateur)
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "select pseudo, statut, mot_de_passe from UTILISATEUR")

                # on récupère des tuples et les transforme en objects Utilisateurs
                result = [Utilisateur(pseudo=item[0], statut=item[1], mot_de_passe=item[2])
                        for item in cur.fetchall()]
                return result
            except psycopg2.Error as error:
                connection.rollback()
                raise error


    def get_util(self, pseudo):
        """
        input :

        * pseudo de l'utilisateur qu'on veut récupérer \\

        Cette fonction permet de récupérer l'utilisateur dont le pseudo est placé en paramètre \\

        output :
        
        * objet Utilisateur
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "select pseudo,  statut, mot_de_passe from UTILISATEUR where pseudo=%s", (pseudo,))

                found = cur.fetchone()
                if found:
                    return Utilisateur(found[0], found[1], found[2])

                return None
            except psycopg2.Error as error:
                connection.rollback()
                raise error