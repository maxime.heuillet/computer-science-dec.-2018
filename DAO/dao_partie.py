from OBJET_METIER.partie import Partie
from AUTRES.connection import connection



import psycopg2


# Table : PARTIE
# Objets : Partie
# on ajoute partie

class DAOpartie :
    """
    Cette classe permet de faire le lien avec la table PARTIE
    """
    def create(self, partie):
        """
        input :
        
        * objet Partie qu'on veut insérer dans la table PARTIE \\

        Cette fonction permet d'insérer dans la table PARTIE l'objet partie placé en paramètre \\

        output :

        * objet Partie (avec cette fois-ci son identifiant)
        """
        cur = connection.cursor()
        try:
            cur.execute(
                "INSERT INTO PARTIE (pseudo, budget_initial, somme_a_atteindre, budget_actualise,avancement,compteur_medecins) VALUES (%s, %s, %s, %s,%s,%s) RETURNING id_partie;", (partie.pseudo, partie.budget_initial, partie.somme_a_atteindre, partie.budget_actualise,partie.avancement,partie.compteur_medecins))

            partie.id_partie = cur.fetchone()[0]
            # la transaction est enregistrée en base
            connection.commit()
        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()

        return partie

    def update(self, part):
        """
        input :
        
        * objet Partie qui contient les nouvelles informations sur la partie \\

        Cette fonction permet de mettre à jour dans la table PARTIE l'objet Partie
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "update PARTIE set budget_actualise=%s, compteur_medecins=%s where id_partie=%s", (part.budget_actualise, part.compteur_medecins, part.id_partie))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def delete(self, idpart):
        """
        input :

        * identifiant de la partie qu'on veut supprimer de la table PARTIE \\

        Cette fonction permet de supprimer dans la table PARTIE la ligne correspondant à la partie dont l'identifiant est placé en paramètre
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "delete from PARTIE where id_partie=%s", (idpart,))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error


    
    def get_one_partie(self, idpart):
        """
        input :
        
        * identifiant de la partie qu'on veut récupérer \\

        Cette fonction permet de récupérer la partie dont l'identifiant est placé en paramètre \\

        output :
        
        * objet Partie
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "select id_partie, pseudo, budget_initial, somme_a_atteindre, budget_actualise, avancement, compteur_medecins from PARTIE where id_partie=%s", (idpart,))

                found = cur.fetchone()
                if found:
                    return Partie(found[1], found[2], found[3], found[4], found[5], found[6], found[0])

                return None
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_pseudo(self, id_partie):
        """
        input : identifiant de la partie dont on veut récupérer le pseudo
        Cette fonction permet de récupérer le pseudo associé à la partie dont l'identifiant est placé en paramètre
        output : pseudo
        """
        with connection.cursor() as cur:
            try:
                cur.execute("select pseudo from partie where id_partie=%s" % (id_partie))
                pseudo = cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(pseudo)

    
    def get_all_parties_utilisateur(self,pseudo):
        """
        input :

        * pseudo du joueur dont on veut récupérer les parties \\

        Cette fonction permet de récupérer les parties du joueur dont le pseudo est placé en paramètre \\

        output :
        
        * liste de parties (list of objects Partie)
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "select id_partie, pseudo, budget_initial, somme_a_atteindre, budget_actualise, avancement, compteur_medecins from PARTIE where pseudo=%s", (pseudo,))
                L=[]
                for item in cur.fetchall() :
                    #print(item)
                    L.append(Partie(item[1], item[2], item[3] , item[4], item[5], item[6], item[0]))
                return(L)
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_all_parties_en_cours_utilisateur(self,pseudo):
        """
        input :
        * pseudo du joueur dont on veut récupérer les parties en cours \\

        Cette fonction permet de récupérer les parties en cours du joueur dont le pseudo est placé en paramètre \\

        output :

        * liste de parties en cours (list of objects Partie)
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "select id_partie, pseudo, budget_initial, somme_a_atteindre, budget_actualise, avancement,compteur_medecins from PARTIE where partie.pseudo=%s and avancement='EC'", (pseudo,))
                L=[]
                for item in cur.fetchall() :
                    #print(item)
                    L.append(Partie(item[1], item[2], item[3] , item[4], item[5], item[6], item[0]))
                return(L)
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_budget_actualise(self, id_partie):
        """
        input :
        
        * identifiant de la partie dont on veut récupérer le budget actualisé \\

        Cette fonction permet de récupérer le budget actualisé au cours d'une simulation dans une partie dont l'identifiant est placé en paramètre \\

        output :
        
        * budget actualisé
        """
        with connection.cursor() as cur:
            try:
                cur.execute("select budget_actualise from partie where id_partie=%s" % (id_partie))
                budget_actualise = cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(budget_actualise)

    def get_somme_a_atteindre(self, id_partie):
        """
        input :
        
        * identifiant de la partie dont on veut récupérer la somme_a_atteindre \\

        Cette fonction permet de récupérer la somme_a_atteindre au cours d'une simulation dans une partie dont l'identifiant est placé en paramètre \\

        output :
        
        * somme_a_atteindre
        """
        with connection.cursor() as cur:
            try:
                cur.execute("select somme_a_atteindre from partie where id_partie=%s" % (id_partie))
                somme_a_atteindre = cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(somme_a_atteindre)

    def update_avancement(self,id_partie,avancement):
        """
        input :

        * id_partie : identifiant de la partie à mettre à jour
        * avancement : son nouvel avancement

        Cette fonction met à jour la partie avec son nouvel avancement dans PARTIE

        """
        try:
            with connection.cursor() as cur:
                
                cur.execute("update PARTIE set avancement=%s where id_partie=%s;", (avancement,id_partie))
                connection.commit()

        except psycopg2.Error as error:
            connection.rollback()
            raise error
        finally:
            cur.close()

    def get_avancement_joueur(self,pseudo,avancement):
        """
        input :

        * pseudo : pseudo du joueur concerné
        * avancement : avancement des parties qu'on veut compter

        Cette fonction permet de compter le nombre de parties du joueur avec l'avancement placé en paramètre

        """
        try:
            with connection.cursor() as cur:
                
                cur.execute("select count(id_partie) from partie where avancement=%s and pseudo=%s;", (avancement,pseudo))
                nb_partie = cur.fetchone()[0]
            return (nb_partie)
        except psycopg2.Error as error:
            connection.rollback()
            raise error
        finally:
            cur.close()





