from OBJET_METIER.maladie import Maladie
from AUTRES.connection import connection

import psycopg2

class DAOmaladie:
    """
    Cette classe permet de faire le lien avec la table MALADIE
    """
    def create(self,maladie):
        """
        input :

        * objet maladie qu'on veut faire entrer dans la table MALADIE \\

        Cette fonction permet d'insérer la maladie dans la table \\

        output :

        * objet maladie (avec cette fois-ci son identifiant)
        """
        cur = connection.cursor()
        try:
            cur.execute("INSERT INTO MALADIE (specialite, nom_mal, nb_jours) VALUES ( %s, %s, %s) RETURNING id_maladie;", ( maladie.specialite,  maladie.nom_mal, maladie.nb_jours))
            
            # la transaction est enregistrée en base
            connection.commit()
            maladie.id_maladie = cur.fetchone()[0]

        except psycopg2.Error as error:
            # la transaction est annulée
            connection.rollback()
            raise error
        finally:
            cur.close()
        return(maladie)

    def generate_one(self):
        """
        Cette fonction permet de sélectionner une maladie au hasard dans la table MALADIE \\

        output :

        * identifiant, spécialité nécessaire, nom et nombre de jours correspondant à la maladie sélectionnée
        """
        with connection.cursor() as cur:
            try:
                cur.execute('SELECT id_maladie,specialite, nom_mal, nb_jours FROM MALADIE ORDER BY RANDOM() LIMIT 1 ;' )
                found=cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(found)

    def get_all_maladies(self):
        """
        Cette fonction permet de récupérer l'ensemble des maladies présentes dans la table MALADIE \\

        output :

        * liste de maladies (list of objects Maladie)
        """
        result = []
        with connection.cursor() as cur:
            try:
                cur.execute("SELECT * FROM MALADIE")
                found = cur.fetchone()
                maladie = Maladie(found[1],found[2],found[3],found[0])
                result.append(maladie)
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        maladie = Maladie(found[1],found[2],found[3],found[0])
                        result.append(maladie)
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        return(result)
            

    
    def update(self,maladie):
        """
        input :

        * maladie qu'on veut mettre à jour dans la table MALADIE, l'objet maladie contient les nouvelles informations \\

        Cette fonction permet de mettre à jour la table MALADIE
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "UPDATE MALADIE SET specialite=%s, nom_mal=%s, nb_jours=%s WHERE id_maladie=%s", (maladie.specialite,maladie.nom_mal, maladie.nb_jours,maladie.id_maladie))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        
    def delete(self,id_maladie):
        """
        input :

        * identifiant de la maladie qu'on veut supprimer \\

        Cette fonction permet de supprimer dans la table MALADIE, la ligne correspondant à la maladie dont l'identifiant est placé en paramètre
        """
        with connection.cursor() as cur:
            try:
                cur.execute("delete from MALADIE where id_maladie=%s", (id_maladie,))
                connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error

    def get_one_maladie(self, id_maladie):
        """
        input :

        * identifiant de la maladie qu'on veut récupérer \\

        Cette fonction permet de récupérer l'identifiant, la spécialité nécessaire, le nom et le nombre de jours correspondant à la maladie dont l'identifiant est placé en paramètre \\

        output :
        
        * identifiant, spécialité nécessaire, nom et nombre de jours correspondant à la maladie
        """
        with connection.cursor() as cur:
            try:
                cur.execute(
                    "select id_maladie, specialite, nom_mal, nb_jours from MALADIE where id_maladie=%s", (id_maladie,))
                found = cur.fetchone()
                if found:
                    return Maladie(found[1], found[2], found[3],found[0])
            except psycopg2.Error as error:
                connection.rollback()
                raise error

        return found