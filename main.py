
from API.API_medecin import API_medecins
from API.API_patient import API_patient

from AUTRES.connection import connection

from DAO.dao_maladie import DAOmaladie
from DAO.dao_medecin import DAOmedecin
from DAO.dao_partie import DAOpartie
from DAO.dao_patient import DAOpatient
from DAO.dao_salle_acheter import DAOsalleacheter
from DAO.dao_salle_fonction import DAOsallefonction
from DAO.dao_specialite import DAOspecialite
from DAO.dao_utilisateur import DAOutilisateur
from DAO.dao_bilan import DAObilan

from OBJET_METIER.maladie import Maladie
from OBJET_METIER.medecin import Medecin
from OBJET_METIER.bilan import Bilan
from OBJET_METIER.partie import Partie
from OBJET_METIER.patient import Patient
from OBJET_METIER.salle_acheter import Salle_A_Acheter
from OBJET_METIER.salle_fonction import Salle_en_fonction
from OBJET_METIER.specialite import Specialite
from OBJET_METIER.utilisateur import Utilisateur

from VUE.affichage_choix_journee import Choix_Journee
from VUE.affichage_patient import Affichage_patient
from VUE.affichage_general import Accueil
from VUE.affichage_maladie import AffichageMaladie
from VUE.affichage_partie import AffichagePartie
from VUE.affichage_general import AffichageGeneral

from SERVICE.lancer_partie import Jeu
from SERVICE.service_medecin import SERVICEmedecin
from SERVICE.service_patient import SERVICEpatient
from SERVICE.service_bilan import SERVICEbilan
from SERVICE.upgrade_couloir import upgrade_couloir
from SERVICE.service_bilan import SERVICEbilan
import SERVICE.supprimer_partie
from SERVICE.supprimer_partie import supprimer_partie

   
accueil=Accueil()

print(accueil.accueil1())

