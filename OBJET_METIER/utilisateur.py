class Utilisateur:
    """
    Cette classe correspond aux utilisateurs du jeu (simples joueurs ou joueurs et administrateurs) \\

        * pseudo : pseudo de l'utilisateur
        * statut : 'J' si c'est un simple joueur ou 'A' s'il est aussi administrateur
        * mot_de_passe : son mot de passe
    """
    def __init__(self,pseudo,statut,mot_de_passe):
        """
        input : \\
        
        * pseudo : pseudo de l'utilisateur
        * statut : 'J' si c'est un simple joueur ou 'A' s'il est aussi administrateur
        * mot_de_passe : son mot de passe

        Permet de créer un objet Utilisateur
        """
        self.pseudo=pseudo
        self.statut=statut
        self.mot_de_passe=mot_de_passe