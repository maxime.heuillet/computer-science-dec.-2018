class Partie :
    """
    Cette classe correspond aux différents parties du jeu \\

        * pseudo : pseudo du joueur à qui appartient la partie (string)
        * budget_initial : somme fournie au départ dans la partie (integer)
        * somme_a_atteindre : somme que le joueur doit atteindre à la fin d'une des journée de cette partie pour gagner (integer)
        * budget_actualise : somme que le joueur a actuellement dans cette partie (integer)
        * avancement : statut de la partie : G : Gagnée / P : Perdue / EC : En Cours (string)
        * compteur_medecins : nombre de médecins que le joueur a acheté depuis le début de la partie (integer)
        * id_partie : identifiant de cette partie
    """
    def __init__(self,pseudo,budget_initial,somme_a_atteindre,budget_actualise,avancement,compteur_medecins=0, id_partie=0):
        """
        input : \\

        * pseudo : pseudo du joueur à qui appartient la partie (string)
        * budget_initial : somme fournie au départ dans la partie (integer)
        * somme_a_atteindre : somme que le joueur doit atteindre à la fin d'une des journée de cette partie pour gagner (integer)
        * budget_actualise : somme que le joueur a actuellement dans cette partie (integer)
        * avancement : statut de la partie : G : Gagnée / P : Perdue / EC : En Cours (string)
        * compteur_medecins : nombre de médecins que le joueur a acheté depuis le début de la partie (integer)
        * id_partie : identifiant de cette partie

        Permet de créer un objet Partie
        """  
        self.id_partie=id_partie
        self.pseudo=pseudo
        self.budget_initial=budget_initial
        self.somme_a_atteindre=somme_a_atteindre
        self.budget_actualise=budget_actualise
        self.avancement=avancement
        self.compteur_medecins=compteur_medecins
        
    def baisser_budget(self,a_enlever):
        """
        input : somme à enlever au budget actuel (integer) \\
        
        Permet d'actualiser le budget
        """
        self.budget_actualise=self.budget_actualise-a_enlever
    
    def augmenter_budget(self,a_ajouter):
        """
        input : somme à ajouter au budget actuel (integer) \\

        Permet d'actualiser le budget
        """
        self.budget_actualise = self.budget_actualise+a_ajouter

    def incrementer_compteur(self):
        """
        Permet de mettre à jour le nombre de médecins achetés depuis le début de la partie
        """
        self.compteur_medecins+=1