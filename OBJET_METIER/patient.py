class Patient:
    """
    Cette classe correspond aux patients du jeu \\

        * id_patient : identifiant du patient
        * id_mal : identifiant de la maladie qu'il a
        * id_partie : identifiant de la partie dans laquelle il intervient
        * id_salle : identifiant de la salle dans laquelle il se trouve
        * nom_patient : nom du patient (string)
        * prenom_patient : son prénom (string)
        * titre_patient : son titre (Mr, Mme...) (string)
        * budget_patient : somme qu'il possède pour se faire soigner (integer)
        * frais_avocat : somme qui pénalisera le joueur s'il meurt (integer)
        * jours_restants : nombre de jours qu'il lui reste à vivre avec cette maladie avant de mourir (integer)
    """
    def __init__(self,id_patient,id_mal,id_partie,id_salle,nom_patient,prenom_patient,titre_patient,budget_patient,frais_avocat,jours_restants):
        """
        input : \\

        * id_patient : identifiant du patient
        * id_mal : identifiant de la maladie qu'il a
        * id_partie : identifiant de la partie dans laquelle il intervient
        * id_salle : identifiant de la salle dans laquelle il se trouve
        * nom_patient : nom du patient (string)
        * prenom_patient : son prénom (string)
        * titre_patient : son titre (Mr, Mme...) (string)
        * budget_patient : somme qu'il possède pour se faire soigner (integer)
        * frais_avocat : somme qui pénalisera le joueur s'il ne peut pas rester dans l'hôpital où s'il meurt (integer)
        * jours_restants : nombre de jours qu'il lui reste à vivre avec cette maladie avant de mourir (integer)

        Permet de créer un objet Patient
        """
        self.id_patient=id_patient
        
        self.nom_patient=nom_patient
        self.prenom_patient=prenom_patient
        self.titre=titre_patient
        self.budget_patient=budget_patient
        self.frais_avocat=frais_avocat
        self.id_mal=id_mal
        self.jours_restants=jours_restants
        self.id_partie=id_partie
        self.id_salle=id_salle

    def __print__(self):
        """
        Permet d'afficher un Patient
        """
        print(self.id_patient,self.id_mal,self.id_partie,self.id_salle,self.nom_patient,self.prenom_patient,self.titre,self.budget_patient,self.frais_avocat, self.jours_restants)      

    def baisser_jour(self):
        """
        Permet de mettre à jour le nombre de jours qu'il lui reste à vivre
        """
        self.jours_restants = self.jours_restants - 1
    
    def supprimer_affectation(self):
        """
        Permet de ne plus l'affecter à une salle
        """
        self.id_salle = None
    
    def baisser_budget(self,a_enlever):
        """
        input : somme à enlever (integer) \\

        Permet de mettre à jour son budget
        """
        self.budget_patient = self.budget_patient - a_enlever