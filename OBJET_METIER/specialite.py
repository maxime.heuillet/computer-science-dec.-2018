class Specialite:
    """
    Cette classe correspond aux spécialités présentes dans le jeu \\

        * specialite : nom de la spécialité (string)
    """
    def __init__(self, specialite):
        """
        input : nom de la spécialité (string) \\

        Permet de créer un objet Specialite
        """
        self.specialite = specialite