class Bilan:
    """
    Cette classe correspond aux objets Bilans résumant les parties gagnées, perdues et en cours des joueurs

        * pseudo : identifiant du joueur auquel correspond le bilan
        * parties_gagnees : nombre de parties gagnées par un joueur
        * parties_perdues : nombre de parties perdues par un joueur
        * parties_en_cours : nombre de parties en cours pour un joueur
        * id_bilan : identifiant de ce bilan
    """
    def __init__(self, pseudo,parties_gagnees,parties_perdues,parties_en_cours,id_bilan=0):
        """
        input :
        
        * pseudo : identifiant du joueur auquel correspond le bilan
        * parties_gagnees : nombre de parties gagnées par un joueur
        * parties_perdues : nombre de parties perdues par un joueur
        * parties_en_cours : nombre de parties en cours pour un joueur
        * id_bilan : identifiant de ce bilan

        Permet de créer un objet Bilan
        """        
        self.id_bilan=id_bilan
        self.pseudo=pseudo
        self.parties_gagnees=parties_gagnees
        self.parties_perdues=parties_perdues
        self.parties_en_cours=parties_en_cours

    def incremente_parties_gagnees(self):
        """
        Permet d'actualiser le nombre de parties gagnées
        """
        self.parties_gagnees=self.parties_gagnees+1

    def incremente_parties_perdues(self):
        """
        Permet d'actualiser le nombre de parties perdues
        """
        self.parties_perdues=self.parties_perdues+1

    def incremente_parties_en_cours(self):
        """
        Permet d'actualiser le nombre de parties en cours
        """
        self.parties_en_cours=self.parties_en_cours+1

    def decremente_parties_en_cours(self):
        """
        Permet d'actualiser le nombre de parties en cours
        """
        self.parties_en_cours=self.parties_en_cours-1