
class Salle_A_Acheter:
    """
    Cette classe correspond aux salles qu'il est possible d'acheter dans le jeu \\

        * specialite : spécialité à laquelle la salle est dédiée (string)
        * nom_salle : nom de la salle (string)
        * id_salle_temp : identifiant de la salle
    """
    def __init__(self, specialite, nom_salle, id_salle_temp=0):
        """
        input : \\
        
        * specialite : spécialité à laquelle la salle est dédiée (string)
        * nom_salle : nom de la salle (string)
        * id_salle_temp : identifiant de la salle

        Permet de créer un objet Salle_A_Acheter
        """
        self.id_salle_temp=id_salle_temp
        self.nom_salle=nom_salle
        self.specialite=specialite

    def __str__(self):
        """
        Permet d'afficher une Salle_A_Acheter
        """
        return '(Nom : %s)' % (self.nom_salle)