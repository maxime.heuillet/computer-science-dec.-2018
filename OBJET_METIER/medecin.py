class Medecin:
    """
    Cette classe correspond aux médecins présents dans le jeu \\
    
        * nom_med : nom du médecin (string)
        * prenom_med : son prénom (string)
        * specialite : sa spécialité, s'il est seulement généraliste c'est None (string)
        * prix_med : prix d'achat du médecin pour le joueur (integer)
        * cout_consultation : somme que doivent payer les patients après être passés dans son cabinet (integer)
        * capacite_traitement : nombre de patients que le médecin peut voir dans une journée (integer)
        * id_salle : identifiant de la salle dans laquelle il se trouve
        * id_partie : identifiant de la partie dans laquelle il intervient
        * id_medecin : identifiant de ce médecin
    """
    def __init__(self, nom_med, prenom_med, specialite, prix_med, cout_consultation, capacite_traitement,id_salle,id_partie,id_medecin=0):
        """
        input : \\

        * nom_med : nom du médecin (string)
        * prenom_med : son prénom (string)
        * specialite : sa spécialité, s'il est seulement généraliste c'est None (string)
        * prix_med : prix d'achat du médecin pour le joueur (integer)
        * cout_consultation : somme que doivent payer les patients après être passés dans son cabinet (integer)
        * capacite_traitement : nombre de patients que le médecin peut voir dans une journée (integer)
        * id_salle : identifiant de la salle dans laquelle il se trouve
        * id_partie : identifiant de la partie dans laquelle il intervient
        * id_medecin : identifiant de ce médecin

        Permet de créer un objet Medecin
        """  
        self.id_medecin=id_medecin

        self.nom_med = nom_med
        self.prenom_med = prenom_med
        self.specialite=specialite

        self.prix_med = prix_med
        self.capacite_traitement = capacite_traitement
        self.cout_consultation = cout_consultation

        self.id_salle=id_salle
        self.id_partie=id_partie   

    def __print__(self):
        """
        Permet d'afficher un Medecin
        """
        print(self.id_medecin, self.nom_med,self.prenom_med,self.specialite,self.prix_med,self.capacite_traitement,self.cout_consultation,self.id_salle,self.id_partie)      
