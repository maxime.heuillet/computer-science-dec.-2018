class Salle_en_fonction:
    """
    Cette classe correspond aux salles en fonction dans les différentes parties du jeu \\

        * id_partie : identifiant de la partie dans laquelle cette salle est présente
        * id_salle_temp : identifiant du type de Salle_A_Acheter auquel elle correspond
        * capacite_attente : capacité de sa salle d'attente (1 pour salle correspondant à une spécialité) (integer)
        * prix : somme que le joueur doit dépenser pour acheter cette salle (integer)
        * id_salle : identifiant de cette salle
    """
    def __init__(self, id_partie,	id_salle_temp,	capacite_attente,	prix, id_salle=0):
        """
        input : \\
        
        * id_partie : identifiant de la partie dans laquelle cette salle est présente
        * id_salle_temp : identifiant du type de Salle_A_Acheter auquel elle correspond
        * capacite_attente : capacité de sa salle d'attente (1 pour salle correspondant à une spécialité) (integer)
        * prix : somme que le joueur doit dépenser pour acheter cette salle (integer)
        * id_salle : identifiant de cette salle

        Permet de créer un objet Salle_en_fonction
        """
        self.id_salle=id_salle
        #Salle.id_salle+=1
        self.id_partie=id_partie
        self.id_salle_temp=id_salle_temp
        self.capacite_attente=capacite_attente
        self.prix=prix

    def __print__(self):
        """
        Permet d'afficher une Salle_en_fonction
        """
        print(self.id_partie,self.id_salle_temp, self.capacite_attente, self.prix)


