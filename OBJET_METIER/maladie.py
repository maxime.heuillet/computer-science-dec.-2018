class Maladie:
    """
    Cette classe correspond aux maladies présentes dans le jeu \\

        * specialite : spécialité nécessaire pour soigner cette maladie, si seul un généraliste est nécessaire c'est None (string)
        * nom_mal : nom de la maladie (string)
        * nb_jours : nombre de jours pendant lesquels le patient peut survivre à cette maladie (integer)
        * id_maladie : identifiant de cette maladie
    """
    def __init__(self, specialite, nom_mal, nb_jours,id_maladie=0):
        """
        input : \\
        
        * specialite : spécialité nécessaire pour soigner cette maladie, si seul un généraliste est nécessaire c'est None (string)
        * nom_mal : nom de la maladie (string)
        * nb_jours : nombre de jours pendant lesquels le patient peut survivre à cette maladie (integer)
        * id_maladie : identifiant de cette maladie

        Permet de créer un objet Maladie
        """       
        self.id_maladie=id_maladie
        self.specialite=specialite
        self.nom_mal = nom_mal
        self.nb_jours = nb_jours