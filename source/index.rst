.. Three Point Hospital documentation master file, created by
   sphinx-quickstart on Tue Nov 13 19:06:00 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Three Point Hospital's documentation!
================================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: AUTRES.connection
   :members:
.. automodule:: API.API_medecin
   :members:
.. automodule:: API.API_patient
   :members:


Les objets métiers
------------------

.. automodule:: OBJET_METIER.maladie
   :members: 
.. automodule:: OBJET_METIER.medecin
   :members: 
.. automodule:: OBJET_METIER.specialite
   :members: 
.. automodule:: OBJET_METIER.patient
   :members: 
.. automodule:: OBJET_METIER.salle_acheter
   :members: 
.. automodule:: OBJET_METIER.salle_fonction
   :members:
.. automodule:: OBJET_METIER.utilisateur
   :members: 
.. automodule:: OBJET_METIER.partie
   :members: 
.. automodule:: OBJET_METIER.bilan
   :members: 
 

Les DAO
-------

.. automodule:: DAO.dao_maladie
   :members:
.. automodule:: DAO.dao_medecin
   :members:
.. automodule:: DAO.dao_specialite
   :members: 
.. automodule:: DAO.dao_patient
   :members: 
.. automodule:: DAO.dao_salle_acheter
   :members: 
.. automodule:: DAO.dao_salle_fonction
   :members:
.. automodule:: DAO.dao_utilisateur
   :members:
.. automodule:: DAO.dao_partie
   :members:
.. automodule:: DAO.dao_bilan
   :members:
 

Les services
------------

.. automodule:: SERVICE.service_medecin
   :members:
.. automodule:: SERVICE.service_patient
   :members:
.. automodule:: SERVICE.lancer_partie
   :members:
.. automodule:: SERVICE.upgrade_couloir
   :members:
.. automodule:: SERVICE.supprimer_partie
   :members:
.. automodule:: SERVICE.service_bilan
   :members:


Les fonctions avec beaucoup d'affichage
---------------------------------------

.. automodule:: VUE.affichage_general
   :members:
.. automodule:: VUE.affichage_maladie
   :members:
.. automodule:: VUE.affichage_partie
   :members:
.. automodule:: VUE.affichage_patient
   :members:
.. automodule:: VUE.affichage_choix_journee
   :members:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

