import unittest
import psycopg2 
from API.API_medecin import API_medecins
from API.API_patient import API_patient

from AUTRES.connection import connection

from DAO.dao_maladie import DAOmaladie
from DAO.dao_medecin import DAOmedecin
from DAO.dao_partie import DAOpartie
from DAO.dao_patient import DAOpatient
from DAO.dao_salle_acheter import DAOsalleacheter
from DAO.dao_salle_fonction import DAOsallefonction
from DAO.dao_specialite import DAOspecialite
from DAO.dao_utilisateur import DAOutilisateur
from DAO.dao_bilan import DAObilan

from OBJET_METIER.maladie import Maladie
from OBJET_METIER.medecin import Medecin
from OBJET_METIER.bilan import Bilan
from OBJET_METIER.partie import Partie
from OBJET_METIER.patient import Patient
from OBJET_METIER.salle_acheter import Salle_A_Acheter
from OBJET_METIER.salle_fonction import Salle_en_fonction
from OBJET_METIER.specialite import Specialite
from OBJET_METIER.utilisateur import Utilisateur

from VUE.affichage_choix_journee import Choix_Journee
from VUE.affichage_patient import Affichage_patient
from VUE.affichage_general import Accueil
from VUE.affichage_maladie import AffichageMaladie
from VUE.affichage_partie import AffichagePartie
from VUE.affichage_general import AffichageGeneral

from SERVICE.lancer_partie import Jeu
from SERVICE.service_medecin import SERVICEmedecin
from SERVICE.service_patient import SERVICEpatient
from SERVICE.service_bilan import SERVICEbilan
from SERVICE.upgrade_couloir import upgrade_couloir
from SERVICE.service_bilan import SERVICEbilan
import SERVICE.supprimer_partie
from SERVICE.supprimer_partie import supprimer_partie

# TESTS UNITAIRES : sur les objets métiers (leurs dao plus précisément)

class Test(unittest.TestCase):

    ### MALADIE ###
    def testmaladie(self):
        # create
        a=Maladie('spe1','mal_test',1)
        DAOmaladie().create(a)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT nom_mal FROM MALADIE")
            for row in cur:
                M.append(row)
        test = ("mal_test",)
        self.assertIn(test,M)

        # get_all_maladies
        test=DAOmaladie().get_all_maladies()
        l1=len(test)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT * FROM MALADIE")
            for row in cur:
                M.append(row)
            l2=len(M)
        self.assertEqual(l1,l2)

        # get_one
        b = DAOmaladie().get_one_maladie(8)
        self.assertEqual(b.nom_mal,"mal_test")

        # update
        anew=Maladie('spe2','mal_test',2,8)
        DAOmaladie().update(anew)
        aget = DAOmaladie().get_one_maladie(8)
        self.assertEqual(aget.specialite,"spe2")

        # delete
        DAOmaladie().delete(8)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT id_maladie FROM MALADIE")
            for row in cur:
                M.append(row)
        test=(8,)
        self.assertNotIn(test,M)

    



    ### MEDECIN ###
    def testmedecin(self):
        # create
        a=Medecin('nom','prenom','spe1',100,100,100,None,0)
        DAOmedecin().create(a)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT nom_med FROM MEDECIN")
            for row in cur:
                M.append(row)
        test = ("nom",)
        self.assertIn(test,M)

        # get_one_medecin
        b = DAOmedecin().get_one_medecin(1)
        self.assertEqual(b.nom_med,"nom")

    
        # get_all_medecins_partie
        test=DAOmedecin().get_all_medecins_partie(0)
        l1=len(test)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT * FROM MEDECIN")
            for row in cur:
                M.append(row)
            l2=len(M)
        self.assertEqual(l1,l2)


        # capacite_corps
        c = DAOmedecin().capacite_corps(0,'spe1')
        self.assertEqual(c,100)

        # compte_medecin_partie
        c= DAOmedecin().compte_medecin_partie(0)
        self.assertEqual(c,1)

        # delete
        DAOmedecin().delete(Medecin('nom','prenom','spe1',100,100,100,None,0,1))
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT nom_med FROM MEDECIN")
            for row in cur:
                M.append(row)
        test=('nom',)
        self.assertNotIn(test,M)


    ### SPECIALITE
    def testspecialite(self):
        # create
        a='spe_test'
        test=(str.lower(a),)
        DAOspecialite().create(a)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT * FROM SPECIALITE")
            for row in cur:
                M.append(row)
        self.assertIn(test,M)
    
        # generate_one
        test=DAOspecialite().generate_one()
        with connection.cursor() as cur :
            M=[]
            cur.execute('SELECT * FROM SPECIALITE')
            for row in cur:
                M.append(row)
        self.assertIn(test,M)

        # delete
        test=('spe_test',)
        DAOspecialite().delete(a)
        with connection.cursor() as cur:
            M=[]
            cur.execute('SELECT * FROM SPECIALITE')
            for row in cur:
                M.append(row)
        self.assertNotIn(test,M)

        # get_all
        test=DAOspecialite().get_all_specialites()
        l1=len(test)
        with connection.cursor() as cur:
            L=[]
            cur.execute('SELECT specialite FROM SPECIALITE')
            for row in cur:
                L.append({"specialite": row[0]})
            l2=len(L)
        self.assertEqual(l1,l2)


    ### PATIENT ###
    def testpatient(self):
        # create
        a=Patient(1,1,0,None,'nom','prenom','titre',600,60,3)
        DAOpatient().create(a)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT id_patient FROM PATIENT")
            for row in cur:
                M.append(row)
        test=(1,)
        self.assertIn(test,M)

        # get_all_patients
        test=DAOpatient().get_all_patients()
        l1=len(test)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT * FROM PATIENT")
            for row in cur:
                M.append(row)
            l2=len(M)
        self.assertEqual(l1,l2)

        # get_all_patients_partie
        test=DAOpatient().get_all_patients_partie(1)
        l1=len(test)
        with connection.cursor() as cur:
            M=[]
            cur.execute("SELECT * FROM PATIENT WHERE id_partie=1")
            for row in cur:
                M.append(row)
            l2=len(M)
        self.assertEqual(l1,l2)


        # information_patient
        spe = DAOpatient().information_patient(1,0)
        self.assertEqual(spe,'spe1')

        # update
        anew=Patient(1,2,0,None,'nom','prenom','titre',600,60,3)
        DAOpatient().update(anew)
        aget = DAOpatient().information_patient(1,0)
        self.assertEqual(aget,"spe2")

        # delete
        DAOpatient().delete(1)
        test=(1,)
        with connection.cursor() as cur:
            M=[]
            cur.execute('SELECT id_patient FROM PATIENT')
            for row in cur:
                M.append(row)
        self.assertNotIn(test,M)



    ### Les tests pour les autres objets ont été faits en tests utilisateurs (ce sont toujours les mêmes types de tests)
    

    ### De même les services ont été testés en test utilisateur (en vérifiant la base de données à la fin des simulations)
    ### Pour les vérifier il faut alors utiliser INSERT_SQL puis lancer une partie et vérifier que la base de données se complète et se met à jour

    ### Nous avons quand même réalisé des tests unitaires sur les services

    def testservice_patient(self):
        # generate_n
        id_partie=0
        n=10
        patient=API_patient(id_partie)
        SERVICEpatient().generate_n(n,id_partie)
        i=0
        while i<n:
            try:
                with connection.cursor() as cur:
                    cur.execute("INSERT INTO PATIENT (id_maladie, id_partie, id_salle, nom_patient, prenom_patient, titre, budget_patient, frais_avocat, jours_restants) VALUES (%s, %s, %s, %s, %s, %s,%s,%s,%s);", (patient.id_mal,patient.id_partie,patient.id_salle,patient.nom_patient,patient.prenom_patient,patient.titre,patient.budget_patient,patient.frais_avocat, patient.jours_restants))
                    connection.commit()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            finally:
                cur.close()
            i=i+1
        self.assertEqual(n,i)

        # capacite_salle
        id_partie=0
        test=SERVICEpatient().capacite_salle(id_partie)
        with connection.cursor() as cur:
            cur.execute("(SELECT id_salle,capacite_attente FROM SALLE_EN_FONCTION WHERE id_salle_temp=0 and id_partie=%s) INTERSECT (SELECT id_salle,capacite_attente FROM MEDECIN JOIN SALLE_EN_FONCTION USING(id_salle) where medecin.id_partie=%s and salle_en_fonction.id_partie=%s);" % (id_partie,id_partie,id_partie))   
            found = cur.fetchall()
        self.assertEqual(test,found)

        # select_free_room
        id_partie=0
        test=SERVICEpatient().select_free_room(id_partie)
        with connection.cursor() as cur:
            cur.execute("(SELECT id_salle,capacite_attente FROM salle_en_fonction WHERE id_salle_temp=0 and id_partie=%s) INTERSECT (SELECT id_salle,capacite_attente from medecin JOIN salle_en_fonction USING(id_salle) where id_salle_temp=0 and medecin.id_partie=%s);" % (id_partie,id_partie))   
            found = cur.fetchall()
        result = [] 
        if found is None :
            self.assertEqual(test,result)
        for data in found:
            result.append([data[0], data[1]-SERVICEpatient().occupation_room(data[0],id_partie) ])
        tjrs_dispo=[]
        for data in result:
            if data[1]>0:
                tjrs_dispo.append(data[0])
        self.assertEqual(test,tjrs_dispo)

        # visualiser_occupation
        id_partie=0
        test=SERVICEpatient().visualiser_occupation(id_partie)
        resultat=[]
        with connection.cursor() as cur:
            cur.execute(" SELECT id_salle,nom_salle  FROM SALLE_EN_FONCTION join salle_a_acheter using(id_salle_temp) WHERE salle_en_fonction.id_partie=%s order by id_salle" % (id_partie)) 
            res=cur.fetchall()
        for i in res:
            resultat.append([i[0],i[1], SERVICEpatient().occupation_room( i[0],id_partie ) ] )
        self.assertEqual(test,resultat)
    
    def testservice_bilan(self):
        # tableau récapitulatif
        daopartie = DAOpartie()
        somme_a_atteindre = daopartie.get_somme_a_atteindre(0)
        budget_actualise = daopartie.get_budget_actualise(0)
        
        tab1 = [somme_a_atteindre, budget_actualise]
        
        with connection.cursor() as cur:
            try:
                cur.execute("select somme_a_atteindre from partie where id_partie=0" )
                somme_a_atteindre2 = cur.fetchone()[0]
                cur.execute("select budget_actualise from partie where id_partie=0" )
                budget_actualise2 = cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        tab2 = [somme_a_atteindre2, budget_actualise2]
        self.assertEqual(tab1,tab2)
    
        # bilan_joueur
        pseudo='testeur'
        daopartie = DAOpartie()
        parties_gagnees1=daopartie.get_avancement_joueur(pseudo,'G')
        parties_perdues1=daopartie.get_avancement_joueur(pseudo,'P')
        parties_en_cours1=daopartie.get_avancement_joueur(pseudo,'EC')
        with connection.cursor() as cur:
            try:
                cur.execute("select count(id_partie) from partie where avancement=%s and pseudo=%s;", ('G',pseudo))
                parties_gagnees2 = cur.fetchone()[0]
                cur.execute("select count(id_partie) from partie where avancement=%s and pseudo=%s;", ('P',pseudo))
                parties_perdues2 = cur.fetchone()[0]
                cur.execute("select count(id_partie) from partie where avancement=%s and pseudo=%s;", ('EC',pseudo))
                parties_en_cours2 = cur.fetchone()[0]
            except psycopg2.Error as error:
                connection.rollback()
                raise error
        tab1 = [parties_gagnees1, parties_perdues1, parties_en_cours1]
        tab2 = [parties_gagnees2, parties_perdues2, parties_en_cours2]
        self.assertEqual(tab1,tab2)
    
    def testservice_medecin(self):
        #traitement generaliste
        id_partie=0
        test=SERVICEmedecin().traitement_generaliste(id_partie)
        daopatient=DAOpatient()
        daopartie=DAOpartie()
        daosef=DAOsallefonction()
        recales=0
        penalite_recales=0
        soignes=0
        consulte=0
        specialiste=0
        recette_medecin=0 
        doctors=[]
        with connection.cursor() as cur:
            try:
                cur.execute("select id_medecin, nom_med, prenom_med, specialite, prix_med, cout_consultation, capacite_traitement, id_salle, medecin.id_partie from medecin join salle_en_fonction using (id_salle) where (id_salle_temp=0 and medecin.id_partie=%s and salle_en_fonction.id_partie=%s) order by capacite_traitement desc;" % (id_partie,id_partie) )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                medecin= Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                doctors.append(medecin)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        medecin = Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                        doctors.append(medecin)
            for medecin in doctors:
                if medecin.id_salle is None:
                    print('%s %s est sans salle' % (medecin.nom,medecin.prenom))
                else:
                    print('\n salle: %s \n nom: %s' % (medecin.id_salle,medecin.nom_med) )
                    journee=SERVICEpatient().travail_generaliste(medecin.id_salle, medecin.capacite_traitement,id_partie)
                    print('composition journee : %s' % (SERVICEmedecin().composition_journee(id_partie, medecin.id_salle,medecin.specialite ) ))
                    for patient in journee:
                        if patient.budget_patient >= medecin.cout_consultation :
                            patient.baisser_budget(medecin.cout_consultation)
                            recette_medecin=recette_medecin+medecin.cout_consultation
                        else :
                            patient.baisser_budget(patient.budget_patient)
                            recette_medecin=patient.budget_patient
                        daopatient.update(patient)
                        consulte=consulte+1
                        if (daopatient.information_patient(patient.id_patient,id_partie) == None) is True:
                            soignes=soignes+1
                            daopatient.delete(patient.id_patient)  
                        else:
                            cpt=daosef.occupation_stocks(id_partie,1) 
                            if cpt < 0:
                                patient.id_salle=daosef.get_id_stocks(id_partie,1)
                                daopatient.update(patient)
                                specialiste=specialiste+1
                            else:
                                recales=recales+1           # si capacité couloir plein, le patient est recalé
                                daopatient.delete(patient.id_patient)
                    print(' soignes: %s \n besoin specialiste:%s \n recette medecin: %s \n ' % (soignes, specialiste,recette_medecin))
            penalite_recales=recales*50
            partie = daopartie.get_one_partie(id_partie)
            partie.augmenter_budget(recette_medecin)
            partie.baisser_budget(penalite_recales)
            daopartie.update(partie)
            print('\n Recette generalistes : %s ' % (recette_medecin) )
            print(' Nombre de recales du couloir : %s \n Penalite : %s' % (recales,penalite_recales))
            print(' Budget actualise : %s  \n' % (partie.budget_actualise))
            L=(soignes,consulte,recette_medecin,recales,penalite_recales)
        self.assertEqual(test,L)
    
        # traitement specialiste
        id_partie=0
        test=SERVICEmedecin().traitement_specialiste(id_partie)
        daopatient = DAOpatient()
        daopartie=DAOpartie()
        doctors=[]
        with connection.cursor() as cur:
            try:
                cur.execute("select id_medecin, nom_med, prenom_med, specialite, prix_med, cout_consultation, capacite_traitement, id_salle, medecin.id_partie from medecin join salle_en_fonction using (id_salle) where (id_salle_temp=0 and medecin.id_partie=%s and salle_en_fonction.id_partie=%s) order by capacite_traitement desc;" % (id_partie,id_partie) )
                found = cur.fetchone()
            except psycopg2.Error as error:
                connection.rollback()
                raise error
            try:
                medecin= Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                doctors.append(medecin)
            except TypeError as error:
                found=None
            else:
                while found is not None:
                    found = cur.fetchone()
                    if found is None:
                        pass
                    else:
                        medecin = Medecin(found[1], found[2], found[3], found[4], found[5], found[6], found[7],found[8],found[0])
                        #medecin.__print__()
                        doctors.append(medecin)
        recette_medecin=0
        soignes=0
        for medecin in doctors:
            if medecin.id_salle is None:
                print('%s %s est sans salle' % (medecin.nom_med,medecin.prenom_med))
            else:
                print('\n salle: %s \n nom: %s' % (medecin.id_salle,medecin.nom_med) )
                #soignes=0
                journee=SERVICEpatient().travail_specialiste(medecin.id_salle,medecin.specialite, medecin.capacite_traitement,id_partie)
                print('composition journee : %s' % (SERVICEmedecin().composition_journee(id_partie,medecin.id_salle,medecin.specialite )))
                for patient in journee:
                    if patient.budget_patient >= medecin.cout_consultation:
                        patient.baisser_budget(medecin.cout_consultation)
                        recette_medecin=recette_medecin+medecin.cout_consultation
                    else :
                        patient.baisser_budget(patient.budget_patient)
                        recette_medecin=recette_medecin+patient.budget_patient                    
                    soignes=soignes+1
                    daopatient.delete(patient.id_patient) 
                print(' patients soignes: %s \n recette medecin: %s \n ' % (soignes,recette_medecin) ) 
        partie = daopartie.get_one_partie(id_partie)
        partie.augmenter_budget(recette_medecin)
        daopartie.update(partie)
        print('\n Recette specialistes : %s ' % (recette_medecin) )
        print(' Budget actualise : %s \n ' % (partie.budget_actualise))
        L=(soignes,recette_medecin)
        self.assertEqual(test,L)


    

if __name__ == '__main__':
    unittest.main()
