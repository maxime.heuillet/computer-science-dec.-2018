from AUTRES.connection import connection
from DAO.dao_salle_fonction import DAOsallefonction
from DAO.dao_partie import DAOpartie
from OBJET_METIER.salle_fonction import Salle_en_fonction
import psycopg2
from PyInquirer import Separator, prompt, Validator, ValidationError


def upgrade_couloir(id_partie):
    """
    input : 
    
    * id_partie : identifiant de la partie dont on veut améliorer le couloir

    Permet d'améliorer la capacité du couloir de la partie, c'est à dire le nombre de patients qu'il peut contenir. Le joueur devra payer le double de la nouvelle capacité du couloir
    """
    daosallefonction=DAOsallefonction()
    daopartie=DAOpartie()
    partie=daopartie.get_one_partie(id_partie)
    budget=partie.budget_actualise
    id_couloir=daosallefonction.get_id_stocks(id_partie,1)
    couloir=daosallefonction.get_one_sallefonction_partie(id_partie,id_couloir)
    capacite_actuelle = couloir.capacite_attente
    print("Vous allez modifier la capacite de votre couloir. \n Le prix d'achat est 2 fois la capacité demandé.\n Vous avez un budget de " + str(budget))
    b=False
    while b==False:
        choix=input("Quelle capacité voulez-vous attribuer au couloir ? On attend un entier ! La capacité actuelle est de " + str(capacite_actuelle) + "\n NA pour annuler \n")
        if choix=="NA":
            b=True
            return()
        else :
            try :
                if int(float(choix))==float(choix):
                    b=True
                    a=False
                    while a==False:
                        cap=int(choix)
                        prix=2*cap
                        if prix>budget:
                            print("Vous n'avez pas assez d'argent")
                            return()
                        
                        if cap <= capacite_actuelle :
                            print ("Vous n'allez quand même pas diminuer la capacité de votre couloir ?!")
                            return()
                        
                        else :
                            print("Vous êtes sur le point de modifier la capacité de votre couloir à " + str(cap) +"\n"+"Au prix de "+ str(prix)+"\n")
                            question_couloir = [
                                {'type':'list',     'name':'Modification du couloir',     'message' : 'Confirmez-vous la modification ?',
                                'choices' : [
                                    'Oui',
                                    Separator(),
                                    'Non'     ]    }]

                            rep=prompt(question_couloir)
                            
                            if rep['Modification du couloir']=='Oui':
                                a=True
                                
                                couloir=Salle_en_fonction(id_partie,0,cap,prix,id_couloir)
                                daosallefonction.update(couloir)
                                partie.baisser_budget(prix)
                                daopartie.update(partie)
                                print("Félicitations, vous venez de modifier votre couloir. \n")
                                return()
                            else :
                                a=True
                                print("Modification annulée")
                                return()
                            
                else:
                    print("La saisie est incorrecte")
            except :
                print("La saisie est incorrecte")