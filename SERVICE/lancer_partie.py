from DAO.dao_partie import DAOpartie
from OBJET_METIER.partie import Partie
from OBJET_METIER.maladie import Maladie
from DAO.dao_salle_acheter import DAOsalleacheter
from DAO.dao_bilan import DAObilan
from DAO.dao_salle_fonction import DAOsallefonction
from DAO.dao_medecin import DAOmedecin
from OBJET_METIER.salle_fonction import Salle_en_fonction
import SERVICE.upgrade_couloir
from SERVICE.upgrade_couloir import upgrade_couloir
from VUE.affichage_choix_journee import Choix_Journee
from SERVICE.service_bilan import SERVICEbilan
import AUTRES.connection
from AUTRES.connection import connection

from SERVICE.service_bilan import SERVICEbilan
from SERVICE.service_medecin import SERVICEmedecin

from PyInquirer import Separator, prompt, Validator, ValidationError

class Jeu :
    """
    Cette classe comporte les fonctions nécessaires au lancement d'une nouvelle partie ou d'une nouvelle journée
    """
    def param_partie(self,budget_init=['600','1000','1500'],somme_atteindre=['2000','2500','3500']):
        """
        input : \\

        * budget_init : trois budgets initiaux sont proposés au joueur, il choisit celui qu'il veut pour sa partie (integer)
        * somme_atteindre : trois somme à atteindre pour gagner la partie sont proposées au joueur qui choisit celle qu'il veut (integer)

        Cette fonction permet d'obtenir les paramètres généraux définissant une partie, c'est le joueur qui les choisit \\

        output : \\

        * budget_initial : le budget initial qu'il a choisi (integer)
        * somme_a_atteindre : la somme à atteindre qu'il a choisie (integer)
        * budget_actualise : le budget actuel du joueur (pour l'instant c'est le budget initial puisqu'il n'a encore rien dépensé ni gagné) (integer)
        """
        question_budget_init = [{'type' : 'list','name':'budget initial','message' : 'Quelle somme voulez-vous au départ ?','choices' : budget_init   }]
        rep_bi = prompt(question_budget_init)
        budget_initial = int(rep_bi['budget initial'])
            
        question_somme_atteindre = [{'type' : 'list','name':'somme à atteindre','message' : 'Quelle somme voulez-vous atteindre pour gagner ?','choices' : somme_atteindre   }]
        rep_sa = prompt(question_somme_atteindre)
        somme_a_atteindre = int(rep_sa['somme à atteindre'])
        
        budget_actualise=budget_initial
        return(budget_initial,somme_a_atteindre,budget_actualise)
    
    def lancer_partie(self,pseudo,NOMS,PRENOMS):
        """
        input : \\

        * pseudo : pseudo du joueur à qui appartient la partie
        * NOMS et PRENOMS : liste des noms et prénoms des médecins de l'API, cela permet de ne les charger qu'au lancement de l'application
        
        Permet de créer les salles essentielles à chaque partie : le couloir et la liste des rendez-vous \\

        Permet de créer la partie et de retourner le lancement d'une nouvelle/première journée
        """
        daopartie=DAOpartie()
        daobilan=DAObilan()
        daosallefonction=DAOsallefonction()
        result=self.param_partie()
        
        bi,sa,ba=result
        bilan=daobilan.get_bilan(pseudo)
        partie=Partie(pseudo,bi,sa,ba,"EC")
        bilan.incremente_parties_en_cours()
        daobilan.update(bilan)
        created_partie=daopartie.create(partie)
        id_partie,sa,ba=created_partie.id_partie,created_partie.somme_a_atteindre,created_partie.budget_actualise
        couloir=Salle_en_fonction(id_partie,1,10,0) # on crée le couloir :capacité de 10 au départ
        daosallefonction.create(couloir)
        liste_RDV = Salle_en_fonction(id_partie,2,5,0) # on la liste des RDV :capacité de 5
        daosallefonction.create(liste_RDV)
        #return(id_partie,sa,ba)
        return(self.lancer_journee(id_partie,NOMS,PRENOMS)) # le budget initial n'a pas d'importance, il sert juste à mettre le premier budget actualisé, après ce qui compte c'est les budgets actualisés et les sommes à atteindre pour gagner
        

    def lancer_journee(self,id_partie,NOMS,PRENOMS):
        """
        input : \\

        * id_partie : identifiant de la partie pour laquelle on veut lancer la nouvelle journée
        * NOMS et PRENOMS : liste des noms et prénoms des médecins de l'API, cela permet de ne les charger qu'au lancement de l'application

        Permet à l'utilisateur de faire ses choix pour la nouvelle journée
        """
        daopartie = DAOpartie()
        partie=daopartie.get_one_partie(id_partie)
        budget_actualise,somme_a_atteindre = partie.budget_actualise, partie.somme_a_atteindre
        if budget_actualise>=somme_a_atteindre:
            print("\nVous avez déjà gagné ;) !\n")
            daopartie.update_avancement(id_partie,"G")
            a=True
            return(a)
        choixjournee=Choix_Journee()
        questions=Choix_Journee.questions_choix
        rep_choix = prompt(questions)
        
        if rep_choix["Choix pour la journée"] == 'Acheter un médecin' :
            choixjournee.choix_medecin_prompt(id_partie,NOMS,PRENOMS)
            return(self.lancer_journee(id_partie,NOMS,PRENOMS))
        
        if rep_choix["Choix pour la journée"] == 'Acheter une salle' :
            choixjournee.choix_salle_prompt(id_partie)
            return(self.lancer_journee(id_partie,NOMS,PRENOMS))
        
        if rep_choix["Choix pour la journée"] == 'Vendre une salle' :
            choixjournee.vendre_salle(id_partie)
            return(self.lancer_journee(id_partie,NOMS,PRENOMS))
        
        if rep_choix["Choix pour la journée"] == 'Vendre un médecin' :
            choixjournee.vendre_medecin(id_partie)
            return(self.lancer_journee(id_partie,NOMS,PRENOMS))
        
        if rep_choix["Choix pour la journée"] == 'Améliorer le couloir' :
            upgrade_couloir(id_partie)
            return(self.lancer_journee(id_partie,NOMS,PRENOMS))
        
        if rep_choix["Choix pour la journée"] == 'Attribuer les salles aux médecins et jouer' :
            return(self.lancer_jeu(id_partie,NOMS,PRENOMS))

        if rep_choix["Choix pour la journée"] == 'Quitter' :
            return(True)

    def lancer_jeu(self,id_partie,NOMS,PRENOMS):
        """
        input : \\
        
        * id_partie : identifiant de la partie pour laquelle on veut lancer la nouvelle journée
        * NOMS et PRENOMS : liste des noms et prénoms des médecins de l'API, cela permet de ne les charger qu'au lancement de l'application

        Lance la simulation et demande au joueur s'il veut continuer (lancer une nouvelle journée) ou continuer plus tard
        """
        a=False
        daopartie=DAOpartie()
        daomedecin = DAOmedecin()
        daobilan=DAObilan()
        service_bilan=SERVICEbilan()
        servmedecin=SERVICEmedecin()
        daosallefonction = DAOsallefonction()
        partie=daopartie.get_one_partie(id_partie)
        if daomedecin.compte_medecin_partie(id_partie) == 0 or daosallefonction.compte_salle_fonction_partie(id_partie) == 0 :
            print("Il vous faut au moins une salle et un médecin !!!")
            return(self.lancer_journee(id_partie,NOMS,PRENOMS))
        
        else:
            while a==False:
                daomedecin.supprimer_affectations_partie(id_partie)
                servmedecin.room_attribution_doctor(id_partie,NOMS,PRENOMS)
                service_bilan.simulation(id_partie)
                service_bilan.donnees_actuelles(id_partie)
                service_bilan.tableau_recapitulatif(id_partie)
                service_bilan.bilan_jour(id_partie)
                daomedecin.supprimer_affectations_partie(id_partie)
                partie=daopartie.get_one_partie(id_partie)
                budget_actualise,somme_a_atteindre = partie.budget_actualise, partie.somme_a_atteindre
                if budget_actualise>=somme_a_atteindre:
                    a=True
                    print(" \n FELICITATIONS, VOUS AVEZ GAGNE !!! \n Vous êtes digne d'être le directeur de cet hopital ! \n ")
                    daopartie.update_avancement(id_partie,"G")
                    pseudo=daopartie.get_pseudo(id_partie)
                    bilan=daobilan.get_bilan(pseudo)
                    bilan.decremente_parties_en_cours()
                    bilan.incremente_parties_gagnees()
                    daobilan.update(bilan)
                    
                else:
                    if budget_actualise<0:
                        a=True
                        print(" \n GAME OVER !!! \n Vous avez fait faillite, songez à laisser les commandes de cet hopital ... \n")
                        daopartie.update_avancement(id_partie,"P")
                        pseudo=daopartie.get_pseudo(id_partie)
                        bilan=daobilan.get_bilan(pseudo)
                        bilan.decremente_parties_en_cours()
                        bilan.incremente_parties_perdues()
                        daobilan.update(bilan)
                    else:
                        questions = [
                        {'type':'list',
                        'name':'Choix de fin de journée',
                        'message' : 'Que voulez-vous faire ?',
                        'choices' : ["Continuer", Separator(), "Continuer plus tard"]    }   ]
                        reponses = prompt(questions)
                        if reponses["Choix de fin de journée"] == "Continuer":
                            a=False
                            return(self.lancer_journee(id_partie,NOMS,PRENOMS))
                        else:
                            a=True
            return(a)

