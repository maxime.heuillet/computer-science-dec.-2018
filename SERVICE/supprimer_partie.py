from DAO.dao_medecin import DAOmedecin
from DAO.dao_salle_fonction import DAOsallefonction
from DAO.dao_patient import DAOpatient
from DAO.dao_partie import DAOpartie

def supprimer_partie(id_partie):
    """
    input :
    
    * id_partie : identifiant de la partie qu'on veut supprimer

    Permet de supprimer une partie et toutes ses dépendances (patients de la partie, salles, médecins, couloir, liste RDV)
    """
    daomedecin = DAOmedecin()
    daosallefonction = DAOsallefonction()
    daopatient = DAOpatient()
    daopartie = DAOpartie()
    
    try :
        liste_patients = daopatient.get_all_patients_partie(id_partie)
        for patient in liste_patients :
            daopatient.delete(patient.id_patient)
    except:
        pass
    try:
        liste_med = daomedecin.get_all_medecins_partie(id_partie)
        for med in liste_med : 
            daomedecin.delete(med)
    except:
        pass
    try :
        liste_salles = daosallefonction.get_all_sallefonction_partie(id_partie)
        for salle in liste_salles :
            daosallefonction.delete(salle.id_salle)
    except:
        pass
    try :
        id_rdv = daosallefonction.get_id_stocks(id_partie,2)
        daosallefonction.delete(id_rdv)
    except:
        pass
    try:
        id_couloir = daosallefonction.get_id_stocks(id_partie,1)
        daosallefonction.delete(id_couloir)
    except:
        pass
    
    daopartie.delete(id_partie)