from AUTRES.connection import connection
from API.API_patient import API_patient
import DAO.dao_patient
from DAO.dao_patient import DAOpatient
from DAO.dao_partie import DAOpartie

from DAO.dao_salle_fonction import DAOsallefonction


from DAO.dao_patient import DAOpatient

from tabulate import tabulate
import psycopg2
import random

class SERVICEpatient:
    """
    Cette classe comporte l'ensemble des fonctions permettant de gérer les parties de la simulation qui concernent les patients
    """
    penalite_RDV=0
    prise_RDV=0
    nb_patients_gene_attente=0
    nb_patients_spe_attente=0

    def generate_n(self,n,id_partie):
        """
        input :

        * n : nombre de patients qu'on veut générer (integer)
        * id_partie : identifiant de la partie dans laquelle on veut les générer

        Permet de générer n patients pour une partie donnée
        """
        self.nb_patients_genere=n
        daopatient = DAOpatient()
        i=0
        while i<n:
            daopatient.create(API_patient(id_partie))
            i=i+1

    def occupation_room(self,id_salle,id_partie):
        """
        input :

        * id_salle : identifiant de la salle qui nous interesse
        * id_partie : identifiant de la partie qui nous interesse

        Permet d'obtenir le nombre de patients présents dans la salle qui nous interesse (pour la partie qui nous interesse) \\
        
        output :
        
        * nombre de patients dans cette salle (integer)

        
        """
        daopatient=DAOpatient()
        return(daopatient.compter_patient_salle(id_partie,id_salle))

    def room_attribution(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie qui nous interesse

        Permet de répartir les patients dans les salles d'attente \\

        output :

        * recales : nombre de patients n'ayant pas pu rester dans l'hôpital (plus de place) (integer)
        * penalite_recales : pénalité due à ces patients (50€ en moins par patient) (integer)

        
        """
        daopatient=DAOpatient()
        daopartie=DAOpartie()

        liste=daopatient.all_patients_to_attribute(id_partie)
        #prise_RDV=0
        recales=0
        for patient in liste:
            disponibilites= self.select_free_room(id_partie) 
            if len(disponibilites)==0:
                recales=recales+1
                daopatient.delete(patient.id_patient)
            else:  
                attribution=random.choice(disponibilites)
                patient.id_salle=attribution
                daopatient.update(patient)

        #self.penalite_RDV=self.prise_RDV*20

        penalite_recales=recales*50
        partie=daopartie.get_one_partie(id_partie)
        partie.budget_actualise=partie.budget_actualise-(penalite_recales)#self.penalite_RDV+
        daopartie.update(partie)
        #print('\n Nombre de prises de RDV : %s Penalite : %s' % (self.prise_RDV,self.penalite_RDV))
        print('Nombre de recales : %s Penalite : %s \n' % (recales,penalite_recales))
        return(recales,penalite_recales)

    def capacite_salle(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie qui nous interesse

        Permet de récupérer les salles de généralistes de cette partie et leur capacité d'attente

        
        """
        daosallefonction = DAOsallefonction()
        return(daosallefonction.capacite_generaliste(id_partie))
    
    def select_free_room(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie qui nous interesse

        Permet de selectionner les salles qui sont encore disponibles et qui peuvent encore recevoir des patients \\

        output : 

        * tjrs_dispo (list)

     
        """
        daosallefonction = DAOsallefonction()
        result = []
        
        found = daosallefonction.select_dispo(id_partie) 
        if found is None :
            return(result)
        for data in found:
            result.append([data[0], data[1]-self.occupation_room(data[0],id_partie) ])
        tjrs_dispo=[]
        for data in result:
            if data[1]>0:
                tjrs_dispo.append(data[0])
        return(tjrs_dispo)

    def visualiser_occupation(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie qui nous interesse

        Permet d'obtenir les salles d'une partie ainsi que leur occupation \\

        output :

        * resultat : liste des salles de la partie et leur occupation (list of list)
        """
        resultat=[]
        daosallefonction = DAOsallefonction()
        res = daosallefonction.get_salles_partie(id_partie)
        for i in res:
            resultat.append([i[0],i[1], self.occupation_room( i[0],id_partie ) ] )
        return(resultat)

    def n_patients(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie qui nous interesse

        Genere un nombre aleatoire de patients adapte a la capacite de l'hopital \\

        output :
        
        * nombre de patients (integer)

        
        """
        hopital=self.capacite_salle(id_partie)
        capacite_totale=0
        for room in hopital:
            capacite_totale=capacite_totale+room[1]
        n_patients=random.randint(capacite_totale-5,capacite_totale+5)
        return(n_patients) 

    def faire_mourir_generaliste(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie qui nous interesse

        Baisse le nombre de jours restants du patient à la fin de la journée, le fait mourir s'il ne lui en reste plus, applique alors les frais d'avocat et supprime le patient de la table PATIENT, met à jour la partie
        
        output :

        * nombre de morts causés par le généraliste (integer)
        * frais avocat associés (integer)

       
        """
        daopatient=DAOpatient()
        daosallefonction = DAOsallefonction()
        daopartie=DAOpartie()

        liste_patients = daopatient.get_all_patients_partie(id_partie)
        morts=0
        frais_avocat=0
        self.nb_patients_gene_attente=daopatient.preparer_mort_generaliste(id_partie)

        if len(liste_patients)==0:
            pass
        else:
            for patient in liste_patients :
                id_salle = patient.id_salle
                try:
                    salle = daosallefonction.get_one_sallefonction_partie(id_partie,id_salle)
                    id_salle_temp = salle.id_salle_temp
                    if id_salle_temp == 2 :
                        patient.baisser_jour()
                        patient.supprimer_affectation()
                        daopatient.update(patient)
                except:
                    pass
                if patient.jours_restants==0:
                    morts=morts+1
                    frais_avocat=frais_avocat+patient.frais_avocat
                    daopatient.delete(patient.id_patient)
        
        

        partie = daopartie.get_one_partie(id_partie)
        partie.baisser_budget( frais_avocat)
        daopartie.update(partie)

        print('\n Nombre de RDV de la veille : %s' % (self.nb_patients_gene_attente) )
        print(' Morts et pertes du jour parmi ceux qui avaient pris un RDV : %s \n Frais engendrés: %s' % (morts,frais_avocat) )
        
        return(morts,frais_avocat)


    def faire_mourir_specialiste(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie qui nous interesse

        Baisse le nombre de jours restants du patient à la fin de la journée, le fait mourir s'il ne lui en reste plus, applique alors les frais d'avocat et supprime le patient de la table PATIENT, met à jour la partie
        
        output :

        * nombre de morts dans le couloir (integer)
        * frais avocat associés (integer)
        
       
        """
        daopatient=DAOpatient()
        daopartie=DAOpartie()
        daosallefonction = DAOsallefonction()
        liste_patients = daopatient.get_all_patients_partie(id_partie)
        morts=0
        frais_avocat=0
        self.nb_patients_spe_attente=daopatient.preparer_mort_specialiste(id_partie)

        if len(liste_patients)==0:
            pass
        else:
            for patient in liste_patients :
                id_salle = patient.id_salle
                try:
                    salle = daosallefonction.get_one_sallefonction_partie(id_partie,id_salle)
                    id_salle_temp = salle.id_salle_temp
                
                    if id_salle_temp == 1 :
                        patient.baisser_jour()
                        daopatient.update(patient)
                except :
                    pass
                if patient.jours_restants==0:
                    morts=morts+1
                    frais_avocat=frais_avocat+patient.frais_avocat
                    daopatient.delete(patient.id_patient)

            partie = daopartie.get_one_partie(id_partie)
            partie.baisser_budget( frais_avocat)
            daopartie.update(partie)

            print('\n Nombre de patients dans le couloir de la veille : %s' % (self.nb_patients_spe_attente) )
            print(' Morts dans le couloir : %s \n Frais engendrés : %s' % (morts,frais_avocat) )
            
        return(morts,frais_avocat)
            

    def information_couloir(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie qui nous interesse

        Permet de récupérer le nombre de patients qui attendent dans le couloir pour cette partie, la capacité du couloir de cette partie, et affiche un résumé des patients présents dans le couloir (leur maladie et la spécialité nécessaire pour les soigner)
        """
        daopatient=DAOpatient()
        found2,found3,found4 = daopatient.recupere_info_couloir(id_partie)

        print('Nombre de patients dans le couloir : %s ' % (found2) )
        print('Capacite couloir : %s ' % (found3))

        print('Repartition des patients dans le couloir:')
        liste_dico = []
        for i in found4:
            dico = {"Nom de la maladie" : i[1], "Spécialité nécessaire" : i[2], "Nombre de patients" : i[3]}
            liste_dico.append(dico)
        print(tabulate(liste_dico, headers="keys", tablefmt='grid'))

        print('\n')


    def cas_journee_pleine(self,journee,id_salle_temp,id_partie,capacite_journaliere):
        """
        input :
        
        * une journee genereree de base i.e liste de patients, id_salle_temp (1 ou 2), identifiant partie,cpt journaliere du medecin considere
        
        Cette fonction s'occupe du cas ou la journee est trop grosse par rapport a la cpt de traitement du medecin. Elle retranche une nouvelle journee de taille = la cpt de traitement du medecin. Ceux qui sont pas selectionnes sont soit envoyes dans la liste des rdv (2) soit dans le couloir (1). Encore faut-il que ces derniers ne soient pas pleins. Si cest le cas ils sont recales de l'hopital \\

        ouput :
        
        * la nouvelle journee, le nb de recales, le nb de rdv

        """
        daosef=DAOsallefonction()
        daopatient = DAOpatient()
        new_journee=random.sample(journee,capacite_journaliere)
        RDV_pris=0
        recales=0
        for patient in journee:
            if patient not in new_journee:
                cpt=daosef.occupation_stocks(id_partie,id_salle_temp)
                if cpt<0:
                    RDV_pris=RDV_pris+1
                    patient.id_salle=daosef.get_id_stocks(id_partie,id_salle_temp)
                    daopatient.update(patient)
                else:
                    recales=recales+1
                    daopatient.delete(patient.id_patient)

        return(new_journee,RDV_pris,recales)
        
    
    
    def travail_generaliste(self,id_salle,capacite_journaliere,id_partie):
        """
        input:
        
        * id_salle du medecin
        * capacite journaliere de ttment du medecin
        * identifiant partie

        Genere dabord une journee de travail correspondant au nb de patients dans la salle dattente. Si cette journee est trop grosse appel cas journee pleine. Calcul du nb de recales, du nb de rdv, et mise a jour du budget \\

        output :
        
        * une journee adaptee au medecin considere sous forme de liste de patients

        """
        daopatient = DAOpatient()
        journee=daopatient.all_patients_salle(id_partie,id_salle)
        print('journee initiale : %s' % (len(journee) ) )
        print('capacite medecin : %s' % (capacite_journaliere) )
        difference=capacite_journaliere-len(journee)
        if difference<0:
            cas=self.cas_journee_pleine(journee,2,id_partie,capacite_journaliere)

            #penalite=20*cas[1]+50*cas[2]  # inutile, deja fait dans service_medecin
            #partie.budget_actualise=partie.budget_actualise-penalite
            print('nouvelle journee : %s' % (len(cas[0]) ) )
            print('Nombre de RDV pris : %s Penalite attente : %s' % (cas[1],cas[1]*20))
            print('Nombre de recales : %s Penalite : %s' % (cas[2],cas[2]*50)) 
            return(cas[0])
        else:
            print("pas besoin de modifier la journée de ce médecin")
            return(journee)

    def travail_specialiste(self,id_salle, specialite, capacite_journaliere,id_partie):
        """
        input:
        
        * id_salle du medecin
        * capacite journaliere de ttment du medecin
        * identifiant de la partie
        
        Genere dabord une journee de travail correspondant au nb de patients dans la salle dattente. Si cette journee est trop grosse appel cas journee pleine. Calcul du nb de recales, du nb de rdv, et mise a jour du budget \\

        output :
        
        * une journee adaptee au medecin considere sous forme de liste de patients

        """
        daopatient = DAOpatient()
        journee=daopatient.all_patients_specialite_pour_specialistes(id_partie,specialite)
        print('journee : %s' % (len(journee) ) )
        print('capacite : %s' % (capacite_journaliere) )
        difference=capacite_journaliere-len(journee)
        if difference<0:
            cas=self.cas_journee_pleine(journee,1,id_partie,capacite_journaliere)
            penalite=50*cas[2] 
            print('nouvelle journee : %s' % (len(cas[0]) ) )
            print('Nombre de recales : %s Penalite : %s' % (cas[2],penalite))
            return(cas[0])
        else:
            print("pas besoin de modifier la journée de ce médecin")
            return(journee)
    
