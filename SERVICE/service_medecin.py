from AUTRES.connection import connection
from DAO.dao_salle_fonction import DAOsallefonction
from DAO.dao_medecin import DAOmedecin
from DAO.dao_patient import DAOpatient
from DAO.dao_partie import DAOpartie
from VUE.affichage_choix_journee import Choix_Journee
from OBJET_METIER.salle_fonction import Salle_en_fonction
from OBJET_METIER.partie import Partie
from SERVICE.service_patient import SERVICEpatient
from PyInquirer import Separator, prompt, Validator, ValidationError
from tabulate import tabulate
import psycopg2

class SERVICEmedecin:
    """
    Cette classe comporte l'ensemble des fonctions permettant de gérer les parties de la simulation qui concernent les médecins
    """
    recette_generalistes=0
    recette_specialistes=0
    soignes_generalistes=0
    soignes_specialistes=0
    consulte=0

    def composition_journee(self,id_partie,id_salle,specialite):
        """
        input :

        * id_partie : identifiant de la partie à laquelle on s'interesse
        * id_salle : identifiant de la salle à laquelle on s'interesse
        * specialite : spécialité qui nous interesse (string)

        Permet d'obtenir les maladies et le nombre de patients atteints, pour une spécialité, une salle et une partie données \\
        
        output :

        * liste de liste de maladies, nombre de patients

        """
        daosalleenfonction = DAOsallefonction()
        resultat=[]
        daosalleenfonction = DAOsallefonction()
        daopatient = DAOpatient()

        salles_generaliste=daosalleenfonction.get_salles_generaliste(id_partie)

        if specialite is None: 
            res = daopatient.compte_maladie_salle(id_partie,id_salle)
            for i in res:
                resultat.append([i[0], i[1] ] )

        elif (specialite is not None) and (id_salle in salles_generaliste):
            res = daopatient.compte_maladie_salle(id_partie,id_salle)
            for i in res:
                resultat.append([i[0], i[1] ] )

        else:
            res = daopatient.compte_maladie_salle_specialite(id_partie,id_salle,specialite)
            for i in res:
                resultat.append([i[0], i[1] ] ) 

        return(resultat)

    def room_attribution_doctor(self,id_partie,NOMS,PRENOMS):
        """
        input :

        * id_partie : identifiant de la partie à laquelle on s'interesse
        * NOMS et PRENOMS : liste des noms et prénoms des médecins de l'API, cela permet de ne les charger qu'au lancement de l'application

        Permet d'attribuer à chaque médecin une salle (quand c'est possible bien-sûr !), cette fonction met aussi à jour la base de données MEDECIN (avec les id_salle)

        """
        daomedecin = DAOmedecin()
        daosalleenfonction = DAOsallefonction()
        choixjournee=Choix_Journee()
        doctors=daomedecin.get_all_medecins_partie(id_partie)
        rooms=daosalleenfonction.get_all_sallefonction_partie(id_partie)

        if len(doctors)==0:
            print('pas de medecins achetes')
            choixjournee.choix_medecin_prompt(id_partie,NOMS,PRENOMS)    
        if len(rooms)==0:
            print('pas de salles achetees')
            choixjournee.choix_salle_prompt(id_partie)

        doctors=daomedecin.get_all_medecins_partie(id_partie)
        rooms=daosalleenfonction.get_all_sallefonction_partie(id_partie)

        print('\n PARTIE AFFECTATION SALLE-MEDECIN \n')

        for i in doctors:

            if i.specialite is None:
                specialite='generaliste'
            else:
                specialite=i.specialite

            print('  \n Dr. %s %s  \n specialite : %s \n capacité de traitement : %s \n salle actuelle: ...' % (i.nom_med, i.prenom_med, specialite, i.capacite_traitement) )

            salles=daosalleenfonction.salles_dispo(id_partie,i.specialite)

            if len(salles)==0:
                if i.specialite==None:
                    print('pas de salle de généraliste disponible pour %s' % (i.prenom_med) )
                else :
                    print('pas de salle disponible pour %s en %s' % (i.prenom_med,i.specialite) )
                checkroomgen=daosalleenfonction.salles_dispo(id_partie,None)
                if len(checkroomgen)>0:
                    print('une salle generaliste est disponible !')
                    rep = self.yes_no("Attribution", "Voulez-vous attribuer " + str(i.prenom_med) + " à une salle de generaliste ?")
                    if rep=='Oui':
                        liste_dico=[]
                        C=[]
                        for k in range(len(checkroomgen)):
                            identifiant = checkroomgen[k]
                            salle = daosalleenfonction.get_one_sallefonction_partie(id_partie,identifiant)
                            dico = {"Numéros des salles disponibles" : str(identifiant), "Capacité d'attente" : str(salle.capacite_attente)}
                            liste_dico.append(dico)
                            C.append(str(checkroomgen[k]))
                        print(tabulate(liste_dico, headers="keys", tablefmt='grid'))
                        question_choix = [{'type' : 'list', 'name':'Choix salle','message' : 'Choisir une salle parmi celles affichees','choices' : C   } ]
                        rep_choix = prompt(question_choix)
                        i.id_salle = rep_choix['Choix salle']
                        daomedecin.update(i)  
                else:
                    print('aucune salle de generaliste en reserve !')
                    print('la journee de votre hopital risque de mal se passer !')
                    print('il faudra se montrer plus strategique lors de vos prochains achats !')

            else:
                liste_dico=[]
                C=[]
                for k in range(len(salles)):
                    identifiant = salles[k]
                    salle = daosalleenfonction.get_one_sallefonction_partie(id_partie,identifiant)
                    dico = {"Numéros des salles disponibles" : str(identifiant), "Capacité d'attente" : str(salle.capacite_attente)}
                    liste_dico.append(dico)
                    C.append(str(salles[k]))
                print(tabulate(liste_dico, headers="keys", tablefmt='grid'))
                question_choix = [{'type' : 'list', 'name':'Choix salle','message' : 'Choisir une salle parmi celles affichees','choices' : C   } ]
                rep_choix = prompt(question_choix)   
                i.id_salle = rep_choix['Choix salle']
                daomedecin.update(i)

    def traitement_generaliste(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie à laquelle on s'interesse

        Permet de soigner les patients grâce aux généralistes présents \\

        output :

        * soignes_generalistes : nombre de patients soignés grâce aux généralistes (integer)
        * consulte : nombre de patients ayant consulté un généraliste (integer)
        * recette_medecin : argent gagné grâce aux généralistes (integer)
        * recales : nombre de patients n'ayant pas pu entrer dans le couloir (integer)
        * penalite_recales : penalité associée au patients recalés du couloir (integer)

        """
        daomedecin=DAOmedecin()
        daopatient=DAOpatient()
        daopartie=DAOpartie()
        servpatient = SERVICEpatient()
        daosef=DAOsallefonction()
        doctors=daomedecin.get_all_generaliste_partie(id_partie)

        recales=0
        penalite_recales=0
        soignes=0
        consulte=0
        recette=0 
          
        for medecin in doctors:

            soignes_med = 0
            recette_medecin = 0
            specialiste=0

            if medecin.id_salle is None:
                print('%s %s est sans salle' % (medecin.nom,medecin.prenom))
            else:

                print('\n salle: %s \n nom: %s' % (medecin.id_salle,medecin.nom_med) )

                journee=servpatient.travail_generaliste(medecin.id_salle, medecin.capacite_traitement,id_partie)
                print('composition journee : %s' % (self.composition_journee(id_partie, medecin.id_salle,medecin.specialite ) ))

                for patient in journee:
                    if patient.budget_patient >= medecin.cout_consultation :
                        patient.baisser_budget(medecin.cout_consultation)
                        recette_medecin=recette_medecin+medecin.cout_consultation
                    else :
                        patient.baisser_budget(patient.budget_patient)
                        recette_medecin=recette_medecin+patient.budget_patient

                    daopatient.update(patient)
                    consulte=consulte+1
                    if (daopatient.information_patient(patient.id_patient,id_partie) == None) is True:
                        soignes_med=soignes_med+1
                        daopatient.delete(patient.id_patient)  
                    else:
                        cpt=daosef.occupation_stocks(id_partie,1) 
                        if cpt < 0:
                            patient.id_salle=daosef.get_id_stocks(id_partie,1)
                            daopatient.update(patient)
                            specialiste=specialiste+1
                        else:
                            recales=recales+1                       # si capacité couloir plein, le patient est recalé
                            daopatient.delete(patient.id_patient)
                soignes = soignes + soignes_med
                recette = recette + recette_medecin


                print(" soignes: %s \n attente d'un specialiste:%s \n recette medecin: %s \n " % (soignes_med, specialiste,recette_medecin))
            
        penalite_recales=recales*50
        partie = daopartie.get_one_partie(id_partie)
        partie.augmenter_budget(recette)
        partie.baisser_budget(penalite_recales)
        daopartie.update(partie)
        print('\n Recette generalistes : %s ' % (recette) )
        print(' Nombre de recales du couloir : %s \n Penalite : %s' % (recales,penalite_recales))
        print(' Budget actualise : %s  \n' % (partie.budget_actualise))
        return (soignes,consulte,recette,recales,penalite_recales)

    def traitement_specialiste(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie à laquelle on s'interesse

        Permet de soigner les patients grâce aux spécialistes présents \\

        output :

        * soignes_specialistes : nombre de patients soignés grâce aux spécialistes (integer)
        * recette_specialistes : argent gagné grâce aux spécialistes (integer)

       
        """
        daomedecin = DAOmedecin()
        daopatient = DAOpatient()
        daopartie=DAOpartie()
        servpatient = SERVICEpatient()


        doctors=daomedecin.get_all_specialiste_partie(id_partie)

        recette=0
        soignes=0
        for medecin in doctors:
            recette_medecin = 0
            soignes_med = 0
            if medecin.id_salle is None:
                print('%s %s est sans salle' % (medecin.nom_med,medecin.prenom_med))

            else:
                print('\n salle: %s \n nom: %s' % (medecin.id_salle,medecin.nom_med) )

                journee=servpatient.travail_specialiste(medecin.id_salle,medecin.specialite, medecin.capacite_traitement,id_partie)
                print('composition journee : %s' % (self.composition_journee(id_partie,medecin.id_salle,medecin.specialite )))
            
                for patient in journee:
                    if patient.budget_patient >= medecin.cout_consultation:
                        patient.baisser_budget(medecin.cout_consultation)
                        recette_medecin=recette_medecin+medecin.cout_consultation
                    
                    else :
                        patient.baisser_budget(patient.budget_patient)
                        recette_medecin=recette_medecin+patient.budget_patient                    

                    soignes_med=soignes_med+1
                    daopatient.delete(patient.id_patient)       
                print(' patients soignes: %s \n recette medecin: %s \n ' % (soignes_med,recette_medecin) ) 

                recette = recette + recette_medecin
                soignes = soignes + soignes_med

        partie = daopartie.get_one_partie(id_partie)
        partie.augmenter_budget(recette)
        daopartie.update(partie)

        print('\n Recette specialistes : %s ' % (recette) )
        print(' Budget actualise : %s \n ' % (partie.budget_actualise))

        return (soignes,recette)


    def yes_no(self,nom,message):
        """
        input :

        * nom : nom associé à la question
        * message : message associé à la question

        Permet de proposer au joueur deux options : Oui ou Non \\

        output :

        * rep[nom] : réponse du joueur

        
        """
        questions = [
                {'type' : 'list','name':nom,'message' : message,'choices' : ['Oui', Separator(), 'Non'] }]
        rep = prompt(questions)
        return(rep[nom])


    def payer_generaliste(self,id_medecin, id_partie):
        """
        input :

        * id_medecin : identifiant du médecin à payer
        * id_partie : identifiant de la partie dont il est question

        Permet de payer un généraliste à la fin de sa journée : son salaire est égal à cinq fois sa capacité de traitement \\

        output :

        * salaire (integer)
        """
        daomedecin = DAOmedecin()
        daopartie=DAOpartie()
        medecin = daomedecin.get_one_medecin(id_medecin)
        partie = daopartie.get_one_partie(id_partie)

        salaire = medecin.capacite_traitement * 5
        partie.baisser_budget(salaire)
        daopartie.update(partie)
        return(salaire)


    def payer_specialiste(self,id_medecin, id_partie):
        """
        input :

        * id_medecin : identifiant du médecin à payer
        * id_partie : identifiant de la partie dont il est question

        Permet de payer un spécialiste à la fin de sa journée : son salaire est égal à trois fois sa capacité de traitement \\

        output :

        * salaire (integer)
        """
        daomedecin = DAOmedecin()
        daopartie=DAOpartie()
        medecin = daomedecin.get_one_medecin(id_medecin)
        partie = daopartie.get_one_partie(id_partie)

        salaire = medecin.capacite_traitement * 3
        partie.baisser_budget(salaire)
        daopartie.update(partie)
        return(salaire)





    