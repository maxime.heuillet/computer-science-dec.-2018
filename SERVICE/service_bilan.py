from DAO.dao_patient import DAOpatient
from SERVICE.service_patient import SERVICEpatient

from DAO.dao_medecin import DAOmedecin
from SERVICE.service_medecin import SERVICEmedecin

from DAO.dao_maladie import DAOmaladie

from DAO.dao_partie import DAOpartie

from DAO.dao_bilan import DAObilan
from OBJET_METIER.bilan import Bilan

from VUE.affichage_choix_journee import Choix_Journee

from AUTRES.connection import connection
from VUE.affichage_patient import Affichage_patient

from tabulate import tabulate

class SERVICEbilan:
    """
    Cette classe comporte l'ensemble des services liés au bilan
    """

    n_patients=0
    morts_gene = 0
    morts_spe = 0
    frais_av_gene = 0
    frais_av_spe = 0
    recette_gene = 0
    recette_spe = 0
    soignes_gene = 0
    soignes_spe = 0
    penalite_RDV = 0
    penalite_recales_hopital = 0
    recales_hopital = 0
    prise_RDV = 0
    consulte = 0
    salaire_verse = 0
    recales_couloir = 0
    penalite_recales_couloir = 0

    

    def simulation(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie pour laquelle on veut lancer la simulation d'une journée

        Permet de réaliser la simulation sur une journée
        """
        daomed=DAOmedecin()
        servpatient= SERVICEpatient()
        servmedecin = SERVICEmedecin()
        affichage_patient = Affichage_patient()
        generalistes=daomed.get_all_generaliste_actif_partie(id_partie)
        
        for medecin in generalistes :
            self.salaire_verse=self.salaire_verse+servmedecin.payer_generaliste(medecin.id_medecin,id_partie)
        
        if len(generalistes)==0:
            print('\n PERDU : un hopital ne peut fonctionner sans generaliste \n')
        
        else:

            print('\n PREPARATION DE LA JOURNEE \n')

            self.morts_gene,self.frais_av_gene=servpatient.faire_mourir_generaliste(id_partie)
            self.morts_spe,self.frais_av_spe=servpatient.faire_mourir_specialiste(id_partie)
            
            n=servpatient.n_patients(id_partie)
            self.n_patients=n
            servpatient.generate_n(n,id_partie)
            print(' \n Nombre de patients generes : %s' % (n))
            
            self.recales_hopital,self.penalite_recales_hopital=servpatient.room_attribution(id_partie)
            #print( servpatient.capacite_salle(id_partie) )

            print('\n Repartition des patients par salle :') 
            print(affichage_patient.occupation_tableau(id_partie))
            print('\n Repartition des patients par maladie :')
            print(affichage_patient.composition_tableau(id_partie))
            
            print(' \n PARTIE GENERALISTE \n')

            self.soignes_gene,self.consulte,self.recette_gene,self.recales_couloir,self.penalite_recales_couloir = servmedecin.traitement_generaliste(id_partie)
            

            specialistes =daomed.get_all_specialiste_actif_partie(id_partie)
            for medecin in specialistes :
                self.salaire_verse = self.salaire_verse + servmedecin.payer_specialiste(medecin.id_medecin,id_partie)
            
            if len(specialistes)==0:
                print('\n Partie specialiste bloquée : pas de specialiste embauche')
            else: 

                print(' \n PARTIE SPECIALISTE \n')

                servpatient.information_couloir(id_partie)
                self.soignes_spe,self.recette_spe = servmedecin.traitement_specialiste(id_partie)
                print('\n Repartition des patients par salle :')
                print(affichage_patient.occupation_tableau(id_partie))
                print('\n Repartition des patients par maladie :')
                print(affichage_patient.composition_tableau(id_partie))
            
            print("\n")

    def donnees_actuelles(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie pour laquelle on veut obtenir les données acutalisées

        Permet d'obtenir les données actualisées d'une partie \\

        output :

        * nombre de patients dans le couloir (integer)
        * argent gagné par l'hopital (integer)
        * argent perdu par l'hopital (integer)
        * recette de l'hopital (integer)
        * nombre de diagnostiques effectués par les médecins (integer)
        * nombre de patients soignés par les médecins (integer)
        """

        daopatient = DAOpatient()

        nb_patients_couloir = daopatient.get_nb_patients_couloir(id_partie)
        
        argent_gagne = self.recette_gene + self.recette_spe
        argent_perdu = self.salaire_verse + self.penalite_recales_hopital + self.penalite_recales_couloir + self.frais_av_gene + self.frais_av_spe
        
        recette = argent_gagne - argent_perdu
        diagnostique = self.consulte + self.soignes_spe
        soignes = self.soignes_gene + self.soignes_spe

        return(nb_patients_couloir,argent_gagne,argent_perdu,recette,diagnostique,soignes)

        


    def tableau_recapitulatif(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie pour laquelle on veut afficher le tableau récapitulatif de la journée qui vient de s'écouler

        Permet d'afficher ce tableau récapitulatif
        """

        print(" \n _________________ BILAN _________________ \n ")

        #recales = self.recales_hopital+self.recales_couloir
        daopartie = DAOpartie()
        somme_a_atteindre = daopartie.get_somme_a_atteindre(id_partie)
        budget_actualise = daopartie.get_budget_actualise(id_partie)
        argent_gagne=self.donnees_actuelles(id_partie)[1]
        argent_perdu=self.donnees_actuelles(id_partie)[2]
        tab1 = ["somme a atteindre", "budget actualisé", "argent gagné", "argent perdu" ]
        tab2 = [somme_a_atteindre, budget_actualise, argent_gagne, argent_perdu]
        liste_tab = [tab1,tab2]
        print(tabulate(liste_tab, tablefmt='grid'))



    def bilan_jour(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie pour laquelle on veut afficher le bilan de la journée qui vient de s'écouler

        Permet d'afficher ce bilan
        """

        nb_patients_couloir,argent_gagne,argent_perdu,recette,diagnostique,soignes=self.donnees_actuelles(id_partie)

        if recette>=0:
            print("Aujourd'hui, votre hôpital vous a rapporté " + str(recette) + " euros. \n")
        else:
            print("Aujourd'hui, votre hôpital vous a fait perdre " + str(recette) + " euros. \n")

        print("La totalité de vos médecins a fait " + str(diagnostique) + " diagnostique(s) et a soigné "+ str(soignes) + " patient(s).") 
        print(" Vous leur avez versé " + str(self.salaire_verse) + " euros de salaire. \n")
        
        print( str(self.n_patients) + " nouveaux patients se sont présentés à votre hôpital. \n")

        print("Vos médecins généralistes ont diagnostiqué " + str(self.consulte) + " patient(s). \n Cela vous a rapporté " + str(self.recette_gene) + " euros")

        if self.soignes_gene==0:
            print("Tous ont eu besoin de consulter un spécialiste")
        else:
            print(" Parmi eux, " + str(self.soignes_gene) + " sont soignés et n'ont pas eu besoin d'un spécialiste.")
        if self.morts_gene==0:
            print(" Félicitations, vos généralistes n'ont laissé mourir aucun patient ! \n")
        else :
            print(" Mais ils ont laissé mourir " + str(self.morts_gene) + " patient(s), cela vous a coûté " + str(self.frais_av_gene) + " euros en frais d'avocat \n") 

        print("Vos spécialistes ont soigné " + str(self.soignes_spe) + " patient(s). \n Cela vous a rapporté " + str(self.recette_spe) + " euros ")

        if self.morts_spe==0:
            print(" Félicitations, vos spécialistes n'ont laissé mourir aucun patient ! \n")
        else:
            print(" Mais ils ont laissé mourir " + str(self.morts_spe) + " patient(s), cela vous a coûté " + str(self.frais_av_spe) + " euros en frais d'avocat \n")

        if self.recales_hopital==0:
            print("Félicitations, vous avez pu accueillir tous les patients qui se sont présentés à votre hôpital !")
        else:
            print( str(self.recales_hopital) + " patients n'ont pas pu entrer dans votre hôpital, cela a engendré " + str(self.penalite_recales_hopital) + " euros en frais de pénalité")
        #if self.prise_RDV==0:
        #    print("Félicitations, aucun patient n'a eu besoin de prendre Rendez-Vous !")
        #else:
        #    print( str(self.prise_RDV) + " ont dû prendre Rendez-Vous pour être traités par un généraliste. \n Cela a engendré " + str(self.penalite_RDV) + " en frais de pénalité.")
        
        if nb_patients_couloir==0:
            print(" Félicitations, aucun patient n'attend dans le couloir ! \n")
        else:
            print(" " + str(nb_patients_couloir) + " attendent patiemment dans le couloir d'être traités par un spécialiste.\n")
            if self.recales_couloir>0:
                print("Attention : " + str(self.recales_couloir) + " n'ont pas pu aller dans le couloir, cela a engendré " + str(self.penalite_recales_couloir) + " euros en frais de pénalité. \n Vous devriez augmenter la capacité de votre couloir" )
            
    

    def bilan_joueur(self,pseudo):
        """
        input :
        
        * pseudo : pseudo du joueur pour lequel on veut afficher les nombres de parties gagnées, perdues et en cours

        Permet d'afficher ce bilan

        output :

        * pseudo du joueur (string)
        """
        daobilan = DAObilan()
        bilan = daobilan.get_bilan(pseudo)
        parties_gagnees=bilan.parties_gagnees
        parties_perdues=bilan.parties_perdues
        parties_en_cours=bilan.parties_en_cours
        tab1 = ["Parties Gagnées", "Parties Perdues", "Parties En Cours"]
        tab2 = [parties_gagnees, parties_perdues, parties_en_cours]
        
        liste_tab = [tab1,tab2]
        print(tabulate(liste_tab, tablefmt='grid'))
        return(pseudo)