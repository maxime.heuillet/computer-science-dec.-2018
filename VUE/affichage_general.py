from PyInquirer import Separator, prompt, Validator, ValidationError
from tabulate import tabulate

from DAO.dao_utilisateur import DAOutilisateur
from DAO.dao_maladie import DAOmaladie
from DAO.dao_specialite import DAOspecialite
from DAO.dao_salle_acheter import DAOsalleacheter
from DAO.dao_partie import DAOpartie
from DAO.dao_patient import DAOpatient
from DAO.dao_bilan import DAObilan

from OBJET_METIER.maladie import Maladie
from OBJET_METIER.utilisateur import Utilisateur
from OBJET_METIER.salle_acheter import Salle_A_Acheter
from OBJET_METIER.partie import Partie
from OBJET_METIER.bilan import Bilan

from AUTRES.connection import connection
import psycopg2

from SERVICE.lancer_partie import Jeu
import SERVICE.supprimer_partie
from SERVICE.supprimer_partie import supprimer_partie
from SERVICE.service_bilan import SERVICEbilan

import API.API_medecin
from API.API_medecin import API_medecins
from PyInquirer import Separator, prompt, Validator, ValidationError

from VUE.affichage_partie import AffichagePartie
from VUE.affichage_maladie import AffichageMaladie

class PasswordValidator(Validator):
    """
    Cette classe gère la validation des mots de passe
    """
    def validate(self, document):
        """
        input :

        * document : mot de passe saisi

        Cette fonction vérifie qu'il contient au moins 6 caractères
        """
        ok = len(document.text) > 5
        if not ok:
            raise ValidationError(
                message='Votre mot de passe doit faire au moins 6 caractères',
                cursor_position=len(document.text))  # Move cursor to end


class Compte :
    """
    Cette classe s'occupe de la gestion des comptes
    """
    questions = [
    {
        'type': 'input',
        'name': 'pseudonyme',
        'message': 'Quel est votre pseudonyme ?',

    },
    {
        'type': 'password',
        'name': 'mot de passe',
        'message': 'Quel est votre mot de passe ?',
        'validate': PasswordValidator
    }
]

    def pseudo_disponible(self,pseudo):
        """
        input :

        * pseudo entré par l'utilisateur (string)

        Cette fonction vérifie qu'il n'est pas déjà pris
        """
        daoutilisateur=DAOutilisateur()
        return not(daoutilisateur.get_util(pseudo))



    def verif_ident(self,pseudo,motdepasse):
        """
        input :

        * pseudo : pseudo entré par l'utilisateur (string)
        * motdepasse : mot de passe entré par l'utilisateur (string)

        La fonction vérifie que le pseudo entré et le mot de passe correspondent \\

        output :

        * booleen (boolean) qui répond True si ça correspond, False sinon
        """
        daoutilisateur=DAOutilisateur()
        if daoutilisateur.get_util(pseudo)!=None:
            if daoutilisateur.get_util(pseudo).mot_de_passe==motdepasse:
                return(True)
            return(False)
        return(False)





    def passer_admin(self,pseudo):
        """
        input :

        * pseudo dont on veut passer le joueur en administrateur

        Cette fonction passe ce simple joueur en administrateur
        """
        daoutilisateur=DAOutilisateur()

        if daoutilisateur.get_util(pseudo)!=None:
            ancien=daoutilisateur.get_util(pseudo)
            utilisateur=Utilisateur(ancien.pseudo,"A",ancien.mot_de_passe) # on le transforme en admin
            daoutilisateur.update(utilisateur)


    def is_admin(self,pseudo):
        """
        input :

        * pseudo dont on veut tester si le propriétaire est administrateur ou simple joueur

        Cette fonction teste si ce joueur est administrateur ou non \\

        output :

        * booléen (boolean) renvoyant True s'il est administrateur, False sinon
        """
        daoutilisateur=DAOutilisateur()

        if daoutilisateur.get_util(pseudo)!=None:
            util=daoutilisateur.get_util(pseudo)
            return(util.statut=="A")


    def creer_admin(self):
        """
        Cette fonction permet de créer un administrateur \\

        NB : NE SERT QU'UNE SEULE FOIS POUR CREER LE PREMIER ADMIN !
        """
        answers = prompt(Compte.questions)
        daoutilisateur=DAOutilisateur()
        daobilan=DAObilan()
        affichage_general = AffichageGeneral()

        if self.pseudo_disponible(answers['pseudonyme']):
            utilisateur=Utilisateur(answers['pseudonyme'],"A", answers['mot de passe'])
            daoutilisateur.create(utilisateur)
            bilan=Bilan(answers['pseudonyme'],0,0,0)
            daobilan.create(bilan)

        else:
            print("{} est déjà utilisé, merci d'en choisir un autre ;) ".format(answers['pseudonyme']))
            rep = affichage_general.yes_no("pseudo déjà utilisé", "Voulez-vous annuler ?" )
            if rep=="Oui":
                return()
            else :
                return self.make_choice()
                

    def make_choice(self):
        """
        Cette fonction permet de créer des joueurs (non administrateurs) \\

        output :

        * "J" (string) : on vient de créer un simple joueur
        * answers['pseudonyme'] : pseudo chosii par le joueur (string)
        """
        answers = prompt(Compte.questions)
        daoutilisateur=DAOutilisateur()
        daobilan=DAObilan()
        affichage_general = AffichageGeneral()

        if self.pseudo_disponible(answers['pseudonyme']):
            utilisateur=Utilisateur(answers['pseudonyme'],"J", answers['mot de passe'])
            daoutilisateur.create(utilisateur)
            bilan=Bilan(answers['pseudonyme'],0,0,0)
            daobilan.create(bilan)
            return("J",answers['pseudonyme'])

        else:
            print("{} est déjà utilisé, merci d'en choisir un autre ;) ".format(answers['pseudonyme']))
            rep = affichage_general.yes_no("pseudo déjà utilisé", "Voulez-vous annuler ?")
            
            if rep=="Oui":
                return()
            else :
                return self.make_choice()
    

    def me_connecter(self):
        """
        Cette fonction permet à l'utilisateur de se connecter, on vérifie que ses identifiants coincident et on lance le menu suivant
        """
        accueil = Accueil()
        compte=Compte()
        answers=prompt(compte.questions)
        rep=self.verif_ident(answers['pseudonyme'],answers['mot de passe'])
        if rep==True:
            Accueil.connect=True
            print("La connection est réussie")
            if self.is_admin(answers['pseudonyme'])==True:
                return(accueil.ecranA(answers['pseudonyme']))
            else:
                return(accueil.ecranJ(answers['pseudonyme']))
        if rep==False:
            print("Il y a une erreur dans votre authentification")
            return(accueil.ecran1())
    
    def promouvoir(self,pseudo):
        """
        input :

        * pseudo : pseudo du joueur qu'on veut promouvoir en administrateur

        Propose les simples joueurs (qu'on peut donc passer en administrateur), l'administrateur en choisit un, ce joueur devient administrateur et on relance le menu principal de l'administrateur
        """
        daoutilisateur = DAOutilisateur()
        utilisateurs = daoutilisateur.get_all_utilisateurs()
        accueil = Accueil()
        C=[]
        for u in utilisateurs :
            if u.statut=="J":
                C.append(u.pseudo)
                C.append(Separator())
        C.append('Vous ne voulez pas promouvoir de joueur')
        questionsA_ecran3 = [{'type':'list','name':'Ecran des joueurs à promouvoir', 'message' : 'Quel joueur voulez-vous promouvoir ?', 'choices' : C}]
        reponsesA_ecran3 = prompt(questionsA_ecran3)

        if reponsesA_ecran3['Ecran des joueurs à promouvoir'] == 'Vous ne voulez pas promouvoir de joueur':
            return(accueil.ecranA(pseudo))
        else:
            u=reponsesA_ecran3['Ecran des joueurs à promouvoir']
            self.passer_admin(u)
            print("Maintenant "+str(u)+" est administrateur !")
            return(accueil.ecranA(pseudo))

class Session:
    def __init__(self):
        self.user = None
        pass

class AbstractVue:
    session = Session()

    def display_info(self):
        pass

    def make_choice(self):
        pass

abstractvue=AbstractVue()




class Accueil(AbstractVue):
    """
    Cette classe contient les différents menus du jeu
    """
    NOMS,PRENOMS = API_medecins()
    questions_admin = [
    {'type':'list',     'name':'Ecran administrateurs',     'message' : 'Que voulez-vous faire ?',
    'choices' : ['Lancer une nouvelle partie',Separator(),'Continuer une partie',Separator(),'Supprimer une partie',Separator(),'Afficher mon Bilan', Separator(),'Modifier le jeu',Separator(),"Promouvoir un joueur au statut d'administrateur",Separator(),'Quitter'    ]    }]

    questions_joueur = [
    {'type':'list',     'name':'Ecran joueurs',     'message' : 'Que voulez-vous faire ?',
    'choices' : ['Lancer une nouvelle partie',Separator(),'Continuer une partie', Separator(),'Supprimer une partie',Separator(),'Afficher mon Bilan',Separator(),'Quitter'    ]    }]

    questions_accueil = [
    { 'type': 'list', 'name': 'Accueil', 'message': 'Que voulez-vous faire ?',
        'choices': ['Me créer un compte',Separator(),'Me connecter',Separator(), 'Quitter'        ]    }]
    
    connect=False

    def display_info(self):
        with open('assets/banner.txt', 'r', encoding="utf-8") as asset:
            print(asset.read())

    def ecran1(self):
        """
        On affiche le premier écran : 3 choix :

        * Créer un compte
        * Se connecter
        * Quitter

        Et en fonction du choix effectué on lance la fonction correspondant
        """
        compte=Compte()
        reponse = prompt(Accueil.questions_accueil)
        if reponse['Accueil'] == 'Me créer un compte':
            return compte.make_choice()

        if reponse['Accueil'] == 'Me connecter':
            return compte.me_connecter()
            
        if reponse['Accueil'] == 'Quitter':
            Accueil.connect="quitt"
            return()

    def ecranJ(self,pseudo):
        """
        On affiche le menu principal des simples joueurs : 4 choix :

        * Lancer une nouvelle partie
        * Continuer une partie
        * Supprimer une partie
        * Afficher son Bilan
        * Quitter

        Et en fonction du choix effectué on lance l'écran suivant
        """
        jeu=Jeu()
        compte = Compte()
        reponseJ = prompt(Accueil.questions_joueur)
        affichage_partie = AffichagePartie()
        servicebilan=SERVICEbilan()
        NOMS,PRENOMS = Accueil.NOMS,Accueil.PRENOMS
        if reponseJ['Ecran joueurs'] == 'Lancer une nouvelle partie':
            a=jeu.lancer_partie(pseudo,NOMS,PRENOMS)
            if a==True :
                return(self.ecranJ(pseudo))
        if reponseJ['Ecran joueurs'] == 'Continuer une partie' :
            pseudo,statut = affichage_partie.continuer_partie(pseudo,"J",NOMS,PRENOMS)
            return(self.ecranJ(pseudo))
        
        if reponseJ['Ecran joueurs'] == 'Supprimer une partie' :
            pseudo,statut = affichage_partie.supprimer_partie(pseudo,"J")
            return(self.ecranJ(pseudo))

        if reponseJ['Ecran joueurs'] == 'Afficher mon Bilan' :
            pseudo = servicebilan.bilan_joueur(pseudo)
            return(self.ecranJ(pseudo))
        
        if reponseJ['Ecran joueurs'] == 'Quitter':
            return(self.ecran1())
    
    def ecranA(self,pseudo):
        """
        On affiche le menu principal des administrateurs, ils ont plus de choix que les simples joueurs : 6 choix :
        
        * Lancer une nouvelle partie
        * Continuer une partie
        * Supprimer une partie
        * Afficher son Bilan
        * Modifier le jeu
        * Promouvoir un joueur en administrateur
        * Quitter

        Et en fonction du choix effectué on lance l'écran suivant
        """
        jeu=Jeu()
        reponseA = prompt(Accueil.questions_admin)
        affichage_maladie=AffichageMaladie()
        servicebilan=SERVICEbilan()
        affichage_partie = AffichagePartie()
        compte = Compte()
        NOMS,PRENOMS = Accueil.NOMS,Accueil.PRENOMS
        if reponseA['Ecran administrateurs'] == 'Lancer une nouvelle partie':
            a=jeu.lancer_partie(pseudo,NOMS,PRENOMS)
            if a==True :
                return(self.ecranA(pseudo))
    
        if reponseA['Ecran administrateurs'] == 'Continuer une partie' :
            pseudo,statut = affichage_partie.continuer_partie(pseudo,"A",NOMS,PRENOMS)
            return(self.ecranA(pseudo))
        
        if reponseA['Ecran administrateurs'] == 'Supprimer une partie' :
            pseudo, statut = affichage_partie.supprimer_partie(pseudo,"A")
            return(self.ecranA(pseudo))
        
        if reponseA['Ecran administrateurs'] == 'Afficher mon Bilan' :
            pseudo = servicebilan.bilan_joueur(pseudo)
            return(self.ecranA(pseudo))



        if reponseA['Ecran administrateurs'] == 'Modifier le jeu' :
            pseudo = affichage_maladie.Modif_jeu(pseudo)
            return(self.ecranA(pseudo))
            
        if reponseA['Ecran administrateurs'] == "Promouvoir un joueur au statut d'administrateur":
            return(compte.promouvoir(pseudo))
    
        if reponseA['Ecran administrateurs'] == 'Quitter':
            return(self.ecran1())


    def accueil1(self):
        """
        On lancer l'écran d'accueil (premier écran) et on regarde si les identifiants saisis correspondant à un simple joueur ou à un administrateur \
       
        Et en fonction du choix effectué on lance l'écran suivant
        """
        statut="J"
        pseudo=""
        while Accueil.connect==False:
            temp=self.ecran1()
            if Accueil.connect==True:
                statut,pseudo=temp[0],temp[1]
                if statut=="A":
                    return(self.ecranA(pseudo))
                else:
                    return(self.ecranJ(pseudo))
            if Accueil.connect=="quitt":
                return()
        

class AffichageGeneral :
    """"
    Cette classe comporte une fonction d'affichage yes/no qui permet au joueur de choisir entre deux choix : Oui ou Non
    """
    def yes_no(self,nom,message):
        """
        input :

        * nom : nom associé à la question
        * message : message associé à la question

        Permet de proposer au joueur deux options : Oui ou Non

        output :
        
        * rep[nom] : réponse du joueur
        """
        questions = [
                {'type' : 'list','name':nom,'message' : message,'choices' : ['Oui', Separator(), 'Non'] }]
        rep = prompt(questions)
        return(rep[nom])