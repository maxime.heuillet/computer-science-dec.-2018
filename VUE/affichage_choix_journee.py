import requests
from tabulate import tabulate
import psycopg2
from AUTRES.connection import connection
from DAO.dao_specialite import DAOspecialite
from DAO.dao_partie import DAOpartie
from DAO.dao_medecin import DAOmedecin
from OBJET_METIER.medecin import Medecin

from OBJET_METIER.salle_acheter import Salle_A_Acheter
from OBJET_METIER.salle_fonction import Salle_en_fonction
from DAO.dao_salle_acheter import DAOsalleacheter
from DAO.dao_salle_fonction import DAOsallefonction


from PyInquirer import Separator, prompt, Validator, ValidationError
import random


class Choix_Journee :

    """
    Cette classe regroupe les fonctions permettant au joueur d'acheter ou vendre une salle ou un médecin

    """
    questions_choix =[ {'type':'list',
                    'name':'Choix pour la journée',
                    'message' : 'Que voulez vous faire ?',
                    'choices' : ['Acheter un médecin', Separator(), 'Vendre un médecin', Separator(), 'Acheter une salle', Separator(), 'Vendre une salle', Separator(), 'Améliorer le couloir', Separator(), 'Attribuer les salles aux médecins et jouer', Separator(), 'Quitter']   }   ]

    def choix_medecin_prompt(self,id_partie,NOMS,PRENOMS,prix=100,cout=20,cap=10):
        """
        input :

        * id_partie : identifiant de la partie pour laquelle on souhaite acheter un médecin
        * NOMS et PRENOMS : liste des noms et prénoms des médecins de l'API, cela permet de ne les charger qu'au lancement de l'application
        * prix, cout et cap : prix d'achat, capacité de traitement et coût de consultation du médecin le moins cher (integer)

        Cette fonction propose au joueur trois médecins : un pas cher, un moyen et un cher, leur capacité de traitement étant croissante avec leur prix \\

        Le joueur choisit d'acheter ou non un des trois médecins proposés. La fonction lance la mise à jour des différentes tables (mise à jour du budget joueur, de la table MEDECIN...)
        """
        daopartie=DAOpartie()
        partie=daopartie.get_one_partie(id_partie)
        budget=partie.budget_actualise
        compteur=partie.compteur_medecins
        daospe=DAOspecialite()
        daomedecin = DAOmedecin()
        if compteur>99:
            print("Il n'y a plus de médecin disponible")
            return()
        else:
            prix1,prix2,prix3=prix,2*prix,3*prix
            cout1,cout2,cout3=cout,2*cout,3*cout
            cap1,cap2,cap3=cap,2*cap,3*cap
            
            SPES=daospe.get_all_specialites()
            
            nom,prenom=NOMS[compteur],PRENOMS[compteur]
            print("Votre nouveau medecin est : \n" + str(nom) +"  " + str(prenom) +"\n")
            S2=[]
            for s in SPES:
                S2.append(s)
                S2.append(Separator())
            S2.append("Aucune spécialité (généraliste)")
            S2.append(Separator())
            S2.append("Vous ne voulez pas acheter de médecin")
            medecins_presents = daomedecin.get_all_medecins_partie(id_partie)
            liste_dico=[]
            for med in medecins_presents :
                dico = {"Prénom" : med.prenom_med, "Nom" : med.nom_med, "Spécialité" : med.specialite}
                liste_dico.append(dico)

            print("Voici un résumé des médecins que vous avez")
            print(tabulate(liste_dico, headers="keys", tablefmt='grid'))

            question_specialite = [
                    {'type':'list','name':'Liste des spécialités','message' : 'A quelle specialité voulez-vous associer ce médecin ?','choices' : S2    }   ]
            reponse_specialite = prompt(question_specialite)
            if reponse_specialite["Liste des spécialités"] == "Vous ne voulez pas acheter de médecin" :
                print("Achat annulé")
                return()
            else :
                question_medecin = [
                    {'type':'list',
                    'name':'Proposition de médecins',
                    'message' : 'Quel médecin voulez vous ? (Vous avez un budget de ' + str(budget)+')' ,
                    'choices' : ["Médecin 1 : Nom : " + str(nom) + "   Prénom : " + str(prenom)+" \n Prix : " + str(prix1) + "   Capacité de traitement : " + str(cap1) + "   cout de consultation : " + str(cout1),Separator(),
                                "Médecin 2 : Nom : " + str(nom) + "   Prénom : " + str(prenom)+" \n Prix : " + str(prix2) + "   Capacité de traitement : " + str(cap2) + "   cout de consultation : " + str(cout2),Separator(),
                                "Médecin 3 : Nom : " + str(nom) + "   Prénom : " + str(prenom)+" \n Prix : " + str(prix3) + "   Capacité de traitement : " + str(cap3) + "   cout de consultation : " + str(cout3),Separator(),
                                "Vous ne voulez pas acheter de médecin" ]   }   ]
                reponse_medecin = prompt(question_medecin)
                if reponse_medecin["Proposition de médecins"] == "Vous ne voulez pas acheter de médecin" :
                    print("Achat annulé")
                    return()

                if reponse_medecin["Proposition de médecins"] == "Médecin 1 : Nom : " + str(nom) + "   Prénom : " + str(prenom)+" \n Prix : " + str(prix1) + "   Capacité de traitement : " + str(cap1) + "   cout de consultation : " + str(cout1) :
                    return(self.achat_medecin(reponse_specialite["Liste des spécialités"],nom,prenom,prix1,cout1,cap1,id_partie,budget,partie))

                if reponse_medecin["Proposition de médecins"] == "Médecin 2 : Nom : " + str(nom) + "   Prénom : " + str(prenom)+" \n Prix : " + str(prix2) + "   Capacité de traitement : " + str(cap2) + "   cout de consultation : " + str(cout2) :
                    return(self.achat_medecin(reponse_specialite["Liste des spécialités"],nom,prenom,prix2,cout2,cap2,id_partie,budget,partie))

                if reponse_medecin["Proposition de médecins"] == "Médecin 3 : Nom : " + str(nom) + "   Prénom : " + str(prenom)+" \n Prix : " + str(prix3) + "   Capacité de traitement : " + str(cap3) + "   cout de consultation : " + str(cout3) :
                    return(self.achat_medecin(reponse_specialite["Liste des spécialités"],nom,prenom,prix3,cout3,cap3,id_partie,budget,partie))


    def achat_medecin(self,reponse,nom,prenom,prix,cout,cap,id_partie,budget,partie) :
        """
        input :

        * reponse : spécialité du médecin qu'on veut créer (None si généraliste) (string)
        * nom et prenom : nom et prénom du médecin qu'on créé (string)
        * prix, cout, cap : son prix d'achat, sa capacité de traitement et son coût de consultation (integer)
        * id_partie : identifiant de la partie pour laquelle on souhaite acheter un médecin
        * budget : argent que le joueur possède dans sa partie (integer)
        * partie : partie dans laquelle on va ajouter ce médecin (objet Partie)
        
        Cette fonction met à jour les différentes tables (mise à jour du budget joueur, de la table MEDECIN...)
        """
        daomedecin=DAOmedecin()
        daopartie=DAOpartie()

        if prix>budget:
            print("Vous n'avez pas assez d'argent pour acheter ce médecin")
            return()
        if reponse == "Aucune spécialité (généraliste)" :
            medecin=Medecin(nom, prenom,None, prix, cout, cap, None, id_partie )
            daomedecin.create(medecin)
            partie.baisser_budget(prix)
            partie.incrementer_compteur()
            daopartie.update(partie)
            print("Félicitations, vous venez d'acheter un médecin généraliste !")
            return()
        else:
            medecin=Medecin(nom, prenom,reponse, prix, cout, cap, None, id_partie )
            daomedecin.create(medecin)
            partie.baisser_budget(prix)
            partie.incrementer_compteur()
            daopartie.update(partie)
            print("Félicitations vous venez d'acheter un médecin !")
            return()


    def choix_salle_prompt(self,id_partie,prixs1=70,prixs2=140,prixs3=210,caps1=10,caps2=20,caps3=30): 
        """
        input :

        * id_partie : identifiant de la partie pour laquelle on souhaite acheter une salle
        * prixs1, prixs2, prixs3 : prix des trois salles qu'on va proposer au joueur (integer)
        * caps1, caps2, caps3 : capacité d'attente des salles proposées (si salles généralistes) (integer)
        
        Si le joueur souhaite une salle généraliste, cette fonction propose au joueur trois salles : une pas chère, une moyenne et une chère, la capacité de leur salle d'attente étant croissante avec leur prix \\

        Si c'est une salle spécialiste, il n'en propose qu'une : celle au prix moyen \\

        Le joueur choisit d'acheter ou non une des salles proposées. La fonction lance la mise à jour des différentes tables (mise à jour du budget joueur, de la table SALLE_EN_FONCTION...)
        """
        daosalleacheter,daopartie=DAOsalleacheter(),DAOpartie()
        daosallefonction=DAOsallefonction()
        list_salle=daosalleacheter.get_all_salleacheter()
        partie=daopartie.get_one_partie(id_partie)
        budget=partie.budget_actualise
        n=len(list_salle)
        
        S=[list_salle[i].specialite for i in range(n) if list_salle[i].specialite != None]
        S1=[]
        for k in S :
            S1.append(k)
            S1.append(Separator())
        S1.append("Aucune spécialité (généraliste)")
        S1.append(Separator())
        S1.append("Vous ne voulez pas acheter de salle")
        salles_presents = daosallefonction.get_all_sallefonction_partie(id_partie)
        liste_dico=[]
        for salle in salles_presents :
            id_salle_temp = salle.id_salle_temp
            spe = daosalleacheter.get_specialite_salle(id_salle_temp)
            if spe is None:
                spe = "aucune spécialité"
            dico = {"Spécialité" : spe}
            liste_dico.append(dico)

            print("Voici un résumé des salles que vous avez")
            print(tabulate(liste_dico, headers="keys", tablefmt='grid'))
        question_specialite = [
                    {'type':'list','name':'Liste des spécialités','message' : 'Pour quelle spécialité voulez-vous acheter une salle ?','choices' : S1}]
        reponse_specialite = prompt(question_specialite)
        if reponse_specialite['Liste des spécialités'] == "Vous ne voulez pas acheter de salle" :
            return("Achat annulé")
        
        if reponse_specialite['Liste des spécialités'] == "Aucune spécialité (généraliste)" :
            question_salle = [
                    {'type':'list','name':'Liste des salles à acheter',
                    'message' : 'Quelle salle voulez-vous acheter ? (Vous avez un budget de ' + str(budget)+')',
                    'choices' : ["Salle 1 : \n    Prix : " + str(prixs1) + "   Capacité d'attente : " + str(caps1),Separator(),
                                "Salle 2 : \n    Prix : " + str(prixs2) + "   Capacité d'attente : " + str(caps2),Separator(),
                                "Salle 3 : \n    Prix : " + str(prixs3) + "   Capacité d'attente : " + str(caps3),Separator(),
                                "Vous ne voulez pas acheter de salle" ]}]

            reponse_salle = prompt(question_salle)
            if reponse_salle['Liste des salles à acheter'] == "Salle 1 : \n    Prix : " + str(prixs1) + "   Capacité d'attente : " + str(caps1) :
                return(self.achat_salle(budget,id_partie,caps1,prixs1,0,partie))
            
            if reponse_salle['Liste des salles à acheter'] == "Salle 2 : \n    Prix : " + str(prixs2) + "   Capacité d'attente : " + str(caps2) :
                return(self.achat_salle(budget,id_partie,caps2,prixs2,0,partie))
            
            if reponse_salle['Liste des salles à acheter'] == "Salle 3 : \n    Prix : " + str(prixs3) + "   Capacité d'attente : " + str(caps3) :
                return(self.achat_salle(budget,id_partie,caps3,prixs3,0,partie))
            
            if reponse_salle['Liste des salles à acheter'] == "Vous ne voulez pas acheter de salle" :
                return()
            
        else :
            spe = reponse_specialite['Liste des spécialités']
            print("Salle : \n    Prix : " + str(prixs2))
            question_achat_spe = [
                    {'type':'list','name':"Proposition d'achat",'message' : 'Voulez-vous acheter cette salle ? (Vous avez un budget de ' + str(budget)+')' + '\n Salle : \n    Prix : ' + str(prixs2),'choices' : ["Oui", Separator(), "Non"]}]
            reponse_achat_spe=prompt(question_achat_spe)
            if reponse_achat_spe["Proposition d'achat"] == "Oui" :
                i=self.get_indice_temp_salle(spe)
                return(self.achat_salle(budget,id_partie,1,prixs2,i,partie)) 
            else :
                print("Vous n'avez pas acheté la salle")
                return()
        
    def achat_salle(self,budget,id_partie,caps,prixs,id_salle_temp,partie):
        """
        input :

        * budget : argent que le joueur possède dans sa partie (integer)
        * id_partie : identifiant de la partie pour laquelle on souhaite acheter une salle
        * caps, prixs : capacité d'attente et prix de la salle qu'on veut créer (integer)
        * id_salle_temp : identifiant du type de salle correspondant à cette salle (dans SALLE_A_ACHETER) (NB : l'id_salle_temp d'une salle généraliste est 0)
        * partie : partie dans laquelle on veut ajouter cette salle (objet Partie)
        
        Cette fonction met à jour les différentes tables (mise à jour du budget joueur, de la table SALLE_EN_FONCTION...)
        """
        daosallefonction=DAOsallefonction()
        daopartie=DAOpartie()
        if prixs>budget:
            print("Vous n'avez pas assez d'argent pour acheter cette salle")
            return()
        salle=Salle_en_fonction(id_partie, id_salle_temp, caps, prixs) 
        daosallefonction.create(salle)
        partie.baisser_budget(prixs)
        daopartie.update(partie)
        print("Félicitations, vous venez d'acheter une salle")
        return()
    

    def get_indice_temp_salle(self,spe):
        """
        input :

        * spe : nom de la spécialité

        Cette fonction permet d'obtenir l'id_salle_temp correspondant aux salles de la spécialité placée en paramètre \\

        output :

        * id_salle_temp (si il a trouvé), sinon liste vide
        """
        list_salle=DAOsalleacheter().get_all_salleacheter()
        n=len(list_salle)
        L=[]
        for i in range(n):
            if list_salle[i].specialite==spe:
                return(list_salle[i].id_salle_temp)
        return L
    
    def vendre_medecin(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie pour laquelle on veut vendre un médecin

        Cette fonction permet de supprimer un médecin que le joueur choisit parmi les médecins de sa partie et de mettre à jour son budget (la revente se fait à la moitié du prix initial)
        """
        daomedecin = DAOmedecin()
        daopartie=DAOpartie()
        liste_med = daomedecin.get_all_medecins_partie(id_partie)
        rep = self.prompt_tableau_medecin(liste_med)
        if rep[0] == "Vous ne voulez pas vendre de médecin" :
            return()
        k = self.recupere_indice(rep[0],rep[1])
        dico = rep[2][k]
        indice = dico["numéro du médecin"]
        medecin=daomedecin.get_one_medecin(indice)
        prix = dico["prix"]
        argent_recupere = int(prix)//2
        daomedecin.delete(medecin)
        partie = daopartie.get_one_partie(id_partie)
        partie.augmenter_budget(argent_recupere)
        daopartie.update(partie)


    def vendre_salle(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie pour laquelle on veut vendre une salle

        Cette fonction permet de supprimer une salle que le joueur choisit parmi les salles de sa partie et de mettre à jour son budget (la revente se fait à la moitié du prix initial)
        """
        daosallefonction = DAOsallefonction()
        daopartie=DAOpartie()
        liste_salles_fonc = daosallefonction.get_all_sallefonction_partie(id_partie)
        rep = self.prompt_tableau_salle(liste_salles_fonc)
        if rep[0] == "Vous ne voulez pas vendre de salle" :
            return()
        k = self.recupere_indice(rep[0],rep[1])
        dico = rep[2][k]
        indice = dico["numéro de la salle"]
        prix = dico["prix"]
        argent_recupere = int(prix)//2
        daosallefonction.delete(indice)
        partie = daopartie.get_one_partie(id_partie)
        partie.augmenter_budget(argent_recupere)
        daopartie.update(partie)


    def prompt_tableau_medecin(self,liste_choix) :
        """
        input :

        * liste_choix : liste_choix est une sortie get_all : une liste d'objets Medecin 

        Cette fonction permet d'afficher un tableau contenant les médecins du joueur et d'en sélectionner un parmi eux \

        output : 

        * rep['Vente médecin'] : identifiant du médecin que le joueur souhaite vendre (integer)
        * C_sans_sep : liste des identifiants de tous les médecins du joueur (list of integer)
        * liste_dico : liste des dictionnaires correspondant aux informations des médecins du joueur (list of dict)
        """
        liste_dico,C,C_sans_sep = [],[],[]
        for med in liste_choix :
            dico = {"numéro du médecin" : med.id_medecin, "nom" : med.nom_med, "prénom" : med.prenom_med, "spécialité" : med.specialite, "prix" : med.prix_med, "cout de consultation" : med.cout_consultation, "capacité de traitement" : med.capacite_traitement }
            C.append(str(med.id_medecin))
            C_sans_sep.append(str(med.id_medecin))
            C.append(Separator())
            liste_dico.append(dico)
        C.append("Vous ne voulez pas vendre de médecin")
        C_sans_sep.append("Vous ne voulez pas vendre de médecin")
        print(tabulate(liste_dico, headers="keys", tablefmt='grid'))
        question_choix = [
            {'type' : 'list', 'name' : "Vente médecin", 'message' :"Quel médecin voulez-vous vendre ? \nIl sera vendu à la moitié de son prix", 'choices' : C }]
        rep = prompt(question_choix)
        return(rep['Vente médecin'],C_sans_sep,liste_dico)


    def prompt_tableau_salle(self,liste_choix) :
        """
        input :

        * liste_choix : liste_choix est une sortie get_all : une liste d'objets Salle_en_fonction

        Cette fonction permet d'afficher un tableau contenant les salles du joueur et d'en sélectionner une parmi elles \\

        output :

        * rep['Vente salle'] : identifiant de la salle que le joueur souhaite vendre (integer)
        * C_sans_sep : liste des identifiants de toutes les salles du joueur (list of integer)
        * liste_dico : liste des dictionnaires correspondant aux informations des salles du joueur (list of dict)
        """
        
        liste_dico,C,C_sans_sep = [],[],[]
        daosalleacheter = DAOsalleacheter()
        for salle in liste_choix :
            spe = daosalleacheter.get_specialite_salle(salle.id_salle_temp)
            dico = {"numéro de la salle" : salle.id_salle, "spécialité" : spe, "capacité d'attente" : salle.capacite_attente, "prix" : salle.prix}
            C.append(str(salle.id_salle))
            C_sans_sep.append(str(salle.id_salle))
            C.append(Separator())
            liste_dico.append(dico)
        C.append("Vous ne voulez pas vendre de salle")
        C_sans_sep.append("Vous ne voulez pas vendre de salle")
        print(tabulate(liste_dico, headers="keys", tablefmt='grid'))
        question_choix = [
            {'type' : 'list', 'name' : "Vente salle", 'message' :"Quelle salle voulez-vous vendre ? \n Elle sera vendue à la moitié de son prix", 'choices' : C }]
        rep = prompt(question_choix)
        return(rep['Vente salle'],C_sans_sep,liste_dico)
    
    def recupere_indice(self,reponse,C_sans_sep):
        """
        input :

        * reponse : identifiant sélectionné par le joueur (integer)
        * C_sans_sep : liste des identifiants (list of integer)

        Cette fonction retrouve l'indice de l'identifiant saisi par le joueur, dans C_sans_sep \\

        output :
        
        * k : indice de l'identifiant (integer)
        """
        for k in range(len(C_sans_sep)) :
            if C_sans_sep[k] == reponse :
                return(k)