from DAO.dao_partie import DAOpartie
from SERVICE.lancer_partie import Jeu
from SERVICE.supprimer_partie import supprimer_partie

from PyInquirer import Separator, prompt, Validator, ValidationError
from tabulate import tabulate

class AffichagePartie:
    """
    Cette classe regroupe les fonctions d'affichage liées aux parties
    """
    def continuer_partie(self,pseudo,statut,NOMS,PRENOMS):
        """
        input :

        * pseudo : pseudo du joueur qui veut continuer une partie (string)
        * statut : son statut : "A" s'il est administrateur et "J" s'il est simple joueur (string)
        * NOMS et PRENOMS : liste des noms et prénoms des médecins de l'API, cela permet de ne les charger qu'au lancement de l'application

        Propose au joueur ses différentes parties, et lance une nouvelle journée (et les choix de début de journée). Si la partie est déjà gagnée ou si le joueur veut quitter, on lance le menu principal du joueur
        """ 
        daopartie,jeu=DAOpartie(),Jeu()
        parties=daopartie.get_all_parties_en_cours_utilisateur(pseudo)
        rep = self.prompt_tableau_partie(parties,"C")
        if rep[0] == 'Vous ne voulez pas continuer de partie':
            return(pseudo,statut)
        else:
            k = self.recupere_indice(rep[0],rep[1])
            dico = rep[2][k]
            indice = dico["numéro de la partie"]
            a=jeu.lancer_journee(indice,NOMS,PRENOMS)
            if a==True:
                return(pseudo,statut)

    def supprimer_partie(self,pseudo,statut):
        """
        input :

        * pseudo : pseudo du joueur qui veut supprimer une partie (string)
        * statut : son statut : "A" s'il est administrateur et "J" s'il est simple joueur (string)

        Propose au joueur ses différentes parties, et supprime celle qu'il veut. Puis on relance le menu principal du joueur
        """ 
        daopartie,jeu=DAOpartie(),Jeu()
        parties=daopartie.get_all_parties_utilisateur(pseudo)
        rep = self.prompt_tableau_partie(parties,"S")
        if rep[0] == 'Vous ne voulez pas supprimer de partie':
            return(pseudo,statut)
        else:
            k = self.recupere_indice(rep[0],rep[1])
            dico = rep[2][k]
            indice = dico["numéro de la partie"]
            supprimer_partie(indice)
            return(pseudo,statut)
    
    def prompt_tableau_partie(self,liste_choix,option) :
        """
        input :

        * liste_choix : liste_choix est une sortie get_all : une liste d'objets Partie*
        * option : "C" pour continuer une partie, "S" pour supprimer une partie

        Cette fonction permet d'afficher un tableau contenant les parties du joueur et d'en sélectionner une parmi elles \\

        output :

        * rep['Ecran parties'] : identifiant de la partie que le joueur souhaite continuer ou supprimer (integer)
        * C_sans_sep : liste des identifiants de toutes les parties du joueur (list of integer)
        * liste_dico : liste des dictionnaires correspondant aux informations des parties du joueur (list of dict)
        """
        
        liste_dico,C,C_sans_sep = [],[],[]
        for partie in liste_choix :
            dico = {"numéro de la partie" : partie.id_partie, "budget actuel" : partie.budget_actualise, "somme à atteindre" : partie.somme_a_atteindre}
            C.append(str(partie.id_partie))
            C_sans_sep.append(str(partie.id_partie))
            C.append(Separator())
            liste_dico.append(dico)
        if option =="C":
            C.append("Vous ne voulez pas continuer de partie")
            C_sans_sep.append("Vous ne voulez pas continuer de partie")
            message = "Quelle partie voulez-vous continuer ?"
        if option =="S":
            C.append("Vous ne voulez pas supprimer de partie")
            C_sans_sep.append("Vous ne voulez pas supprimer de partie")
            message = "Quelle partie voulez-vous supprimer ?"
        print(tabulate(liste_dico, headers="keys", tablefmt='grid'))
        question_choix = [
            {'type' : 'list', 'name' : "Ecran parties", 'message' :message, 'choices' : C }]
        rep = prompt(question_choix)
        return(rep['Ecran parties'],C_sans_sep,liste_dico)
    
    def recupere_indice(self,reponse,C_sans_sep):
        """
        input :

        * reponse : identifiant sélectionné par le joueur (integer)
        * C_sans_sep : liste des identifiants (list of integer)

        Cette fonction retrouve l'indice de l'identifiant saisi par le joueur, dans C_sans_sep \\

        output :
        
        * k : indice de l'identifiant (integer)
        """
        for k in range(len(C_sans_sep)) :
            if C_sans_sep[k] == reponse :
                return(k)