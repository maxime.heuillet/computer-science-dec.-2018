from SERVICE.service_patient import SERVICEpatient
from DAO.dao_patient import DAOpatient
from DAO.dao_maladie import DAOmaladie
from tabulate import tabulate

class Affichage_patient():
    """
    Cette classe regroupe les fonctions d'affichage destinées aux patients
    """
    def occupation_tableau(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie à laquelle on s'interesse

        Cette fonction permet de visualiser les occupations des différentes salles de la partie sous la forme d'un tableau \\

        output :

        * objet Tabulate
        """
        servpatient = SERVICEpatient()
        liste = servpatient.visualiser_occupation(id_partie)
        liste_dico=[]
        for repartition in liste :
            dico = {"occupation de la salle" : repartition[2],"numéro de la salle" : repartition[0],"nom salle":repartition[1]}
            liste_dico.append(dico)
        return(tabulate(liste_dico, headers="keys", tablefmt='grid'))
    
    def composition_tableau(self,id_partie):
        """
        input :

        * id_partie : identifiant de la partie à laquelle on s'interesse

        Cette fonction permet de visualiser les maladies des patients ainsi que le nombre de patients touchés par les différentes maladies sous la forme d'un tableau \\

        output :
        
        * objet Tabulate
        """
        daopatient = DAOpatient()
        daomaladie = DAOmaladie()
        liste = daopatient.composition_patient(id_partie)
        liste_dico=[]
        for maladie in liste :
            id_maladie = maladie[0]
            mal = daomaladie.get_one_maladie(id_maladie)
            dico = {"maladie" : mal.nom_mal, "nombre de patients avec cette maladie" : maladie[1]}
            liste_dico.append(dico)
        return(tabulate(liste_dico, headers="keys", tablefmt='grid'))
