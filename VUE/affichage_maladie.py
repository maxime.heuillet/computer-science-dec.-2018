from DAO.dao_maladie import DAOmaladie
from DAO.dao_patient import DAOpatient
from DAO.dao_salle_acheter import DAOsalleacheter
from DAO.dao_specialite import DAOspecialite

from OBJET_METIER.maladie import Maladie
from OBJET_METIER.salle_acheter import Salle_A_Acheter

from PyInquirer import Separator, prompt, Validator, ValidationError
from tabulate import tabulate

class AffichageMaladie():
    """
    Cette classe regroupe les fonctions d'affichage liées aux maladies
    """
    def Ajouter_maladie(self,pseudo):
        """
        input :

        * pseudo du joueur (integer)

        Cette fonction demande à l'utilisateur les caractéristiques de la nouvelle maladie et la crée \\

        Et on reboucle sur le menu de modification du jeu
        """
        daomaladie=DAOmaladie()
        nom=input("Quel est le nom de la nouvelle maladie ? NA pour annuler l'ajout d'une maladie\n")
        if nom == "NA" :
            return(self.Modif_jeu(pseudo))
        g=daomaladie.get_all_maladies()
        L=[]
        for m in g :
            L.append(str.lower(m.nom_mal))
        if str.lower(nom) in L :
            print("Cette maladie est déjà présente")
            return(self.Modif_jeu(pseudo))
        spe = self.questions_specialite()
        nb_jours = (input("Combien de jours peut-on y survivre ? NA pour annuler l'ajout d'une maladie \n"))
        try:
            nb_jours = int(nb_jours)
        except :
            if nb_jours != "NA" :
                print("Ce n'est pas un entier !")
            return(self.Modif_jeu(pseudo))
        maladie=Maladie(spe,nom,nb_jours)
        daomaladie.create(maladie)
        return(self.Modif_jeu(pseudo))


    def Modifier_maladie(self,pseudo):
        """
        input :

        * pseudo du joueur (integer)

        Cette fonction demande à l'utilisateur quelle maladie modifier puis ses nouvelles caractéristiques et la modifie \\

        Et on reboucle sur le menu de modification du jeu
        """
        daomaladie=DAOmaladie()
        m = daomaladie.get_all_maladies()
        C=[]
        for mal in m :
            C.append(mal)
    
        rep = self.prompt_tableau_maladie(C,"M")

        if rep[0] == "Vous ne voulez pas modifier de maladie":
            return(self.Modif_jeu(pseudo))
        else:
            k = self.recupere_indice(rep[0],rep[1])
            maladie= self.recupere_info_maladie(k,rep[2])
            id_maladie=maladie.id_maladie
            nom=input("Quel est son nouveau nom ?")
            g=daomaladie.get_all_maladies()
            L=[]
            for m in g :
                L.append(str.lower(m.nom_mal))
            if str.lower(nom) in L :
                print("Cette maladie est déjà présente")
            spe = self.questions_specialite()
            try:
                nb_jours=int(input("Combien de jours peut-on y survivre ?"))
            except :
                print("Ce n'est pas un entier !")
                return(self.Modifier_maladie(pseudo))
            
            newmaladie=Maladie(spe,nom,nb_jours,id_maladie)
            daomaladie.update(newmaladie)
            return(self.Modif_jeu(pseudo))


    def Supprimer_maladie(self,pseudo):
        """
        input :

        * pseudo du joueur (integer)

        Cette fonction demande à l'utilisateur quelle maladie il veut supprimer et la supprime, ainsi que ses dépendances (les patients qui ont cette maladie) \\

        Et on reboucle sur le menu de modification du jeu
        """
        daomaladie,daopatient=DAOmaladie(),DAOpatient()
        m = daomaladie.get_all_maladies()
        C=[]
        liste_patients=daopatient.get_all_patients()
        for mal in m :
            C.append(mal)
        rep = self.prompt_tableau_maladie(C,"S")
        if rep[0] == "Vous ne voulez pas supprimer de maladie":
            return(self.Modif_jeu(pseudo))
        else:
            k = self.recupere_indice(rep[0],rep[1])
            maladie= self.recupere_info_maladie(k,rep[2])
            id_maladie=maladie.id_maladie
            for patient in liste_patients :
                if patient.id_mal == id_maladie:
                    daopatient.delete(patient.id_patient)
            daomaladie.delete(id_maladie)
            return(self.Modif_jeu(pseudo))

    
    def yes_no(self,nom,message):
        """
        input :

        * nom : nom associé à la question
        * message : message associé à la question

        Permet de proposer au joueur deux options : Oui ou Non \\

        output :

        * rep[nom] : réponse du joueur
        """
        questions = [
                {'type' : 'list','name':nom,'message' : message,'choices' : ['Oui', Separator(), 'Non'] }]
        rep = prompt(questions)
        return(rep[nom])

    def prompt_tableau_maladie(self,liste_choix,option) :
        """
        input :

        * liste_choix : liste_choix est une sortie get_all : une liste d'objets Maladie
        * option : "M" pour modifier une maladie et "S" pour Supprimer

        Cette fonction permet d'afficher un tableau contenant les infomations sur les maladies du jeu, et d'en sélectionner une parmi elles \\

        output :

        * rep[name] : nom de la maladie que le joueur souhaite modifier ou supprimer (string)
        * C_sans_sep : liste des noms de toutes les maladies du jeu (list of string)
        * liste_dico : liste des dictionnaires correspondant aux informations des maladies du jeu (list of dict)
        """

        liste_dico = []
        C = []
        C_sans_sep=[]
        message=""
        for mal in liste_choix :
            dico = {"numéro de la maladie" : mal.id_maladie, "nom de la maladie" : mal.nom_mal, "spécialité" : mal.specialite, "nombre de jours" : mal.nb_jours}
            C.append(mal.nom_mal)
            C_sans_sep.append(mal.nom_mal)
            C.append(Separator())
            liste_dico.append(dico)
        if option == "M" :
            C.append("Vous ne voulez pas modifier de maladie")
            C_sans_sep.append("Vous ne voulez pas modifier de maladie")
            name = "Modification des maladies"
            message = 'Voici la liste des maladies que vous pouvez modifier \nCes changements ne seront effectifs que pour les nouvelles parties'
        if option == "S" :
            C.append("Vous ne voulez pas supprimer de maladie")
            C_sans_sep.append("Vous ne voulez pas supprimer de maladie")
            name = 'Suppression des maladies'
            message = 'Voici la liste des maladies que vous pouvez supprimer \n Attention !!! Si vous supprimez une maladie, les patients ayant cette maladie seront également supprimés !'
        print(tabulate(liste_dico, headers="keys", tablefmt='grid'))
        question_choix = [
                {'type' : 'list', 'name' : name, 'message' : message,'choices' : C }]
        rep = prompt(question_choix)
        return(rep[name],C_sans_sep,liste_dico)
    
    


    def recupere_indice(self,reponse,C_sans_sep):
        """
        input :

        * reponse : identifiant sélectionné par le joueur (integer)
        * C_sans_sep : liste des identifiants (list of integer)

        Cette fonction retrouve l'indice de l'identifiant saisi par le joueur, dans C_sans_sep \\

        output :

        * k : indice de l'identifiant (integer)
        """
        for k in range(len(C_sans_sep)) :
            if C_sans_sep[k] == reponse :
                return(k)

    
    def recupere_info_maladie(self,k,liste_dico):
        """
        input :

        * k : indice de l'identifiant de la maladie sélectionnée par le joueur, dans liste_dico
        * liste_dico : liste des dictionnaires correspondant aux différentes maladies proposées au joueur

        Cette fonction permet de récupérer la maladie correspondant à l'identifiant sélectionné par le joueur \\

        output :

        * objet Maladie
        """
        dico = liste_dico[k]
        maladie = Maladie(dico["spécialité"],dico["nom de la maladie"],dico["nombre de jours"],dico["numéro de la maladie"])
        return(maladie)
    


    def questions_specialite(self):
        """
        Demande au joueur la spécialité nécessaire pour soigner une maladie, créé la spécialité et la salle correspondantes si elles n'existent pas encore \\

        output :

        * spe : spécialité dont on a besoin pour la maladie (string)
        """
        speyesno = self.yes_no("spécialité","A-t-on besoin d'une spécialité pour cette maladie ?")
        if speyesno == "Non":
            spe = None
            salle = "salle de generaliste"
            daosalleacheter=DAOsalleacheter()
            salle_achat=Salle_A_Acheter(spe, salle)
            daosalleacheter.create(salle_achat)
        else :
            print("Les spécialités déjà présentes sont : \n ")
            daospecialite=DAOspecialite()
            g = daospecialite.get_all_specialites_objet()
            for s in g :
                print(s.specialite)
            spe = input("Quelle est la spécialité nécessaire pour la soigner ? Vous pouvez entrer une spécialité qui n'existe pas encore\n")
            daospecialite.create(spe)
            salle = "salle de " + str(spe)
            daosalleacheter=DAOsalleacheter()
            salle_achat=Salle_A_Acheter(spe,salle)
            daosalleacheter.create(salle_achat)
        return(spe)
    


    def Modif_jeu(self,pseudo):
        """
        input :

        * pseudo : pseudo du joueur qui veut modifier le jeu (c'est un administrateur)

        Propose les différentes modifications possibles du jeu à l'administrateur :

        * Ajouter une maladie
        * Modifier une maladie
        * Supprimer une maladie

        Et lance la fonction correspondant \\
        
        Si l'administrateur ne souhaite pas faire de modifications finalement, on relance le menu principal de l'administrateur
        """
        questionsA_ecran4 = [
                {'type' : 'list',
                'name':'Ecran des modifications',
                'message' : 'Quelle modification voulez-vous faire ?',
                'choices' : ['Ajouter une maladie',Separator(),'Modifier une maladie',Separator(),'Supprimer une maladie',Separator(),'Vous ne voulez pas modifier le jeu']
                }]
        
        reponsesA_ecran4 = prompt(questionsA_ecran4)
        if reponsesA_ecran4['Ecran des modifications']=='Vous ne voulez pas modifier le jeu':
            return(pseudo)
        if reponsesA_ecran4['Ecran des modifications']=='Ajouter une maladie':
            return(self.Ajouter_maladie(pseudo))
        if reponsesA_ecran4['Ecran des modifications']=='Modifier une maladie':
            return(self.Modifier_maladie(pseudo))
        if reponsesA_ecran4['Ecran des modifications']=='Supprimer une maladie':
            return(self.Supprimer_maladie(pseudo))

