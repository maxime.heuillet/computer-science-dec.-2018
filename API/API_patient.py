import requests
import random
from DAO.dao_maladie import DAOmaladie
from OBJET_METIER.patient import Patient

def API_patient(id_partie):
    """
    input :

    * identifiant de la partie dans laquelle apparaîtra ce patient \\

    Cette fonction permet de générer un patient grâce à une API (random user). On récupère son nom, son prénom, son titre (Mr, Mme...), et on lui ajoute les paramètres manquants afin de compléter l'objet patient (frais d'avocat...) \\

    output :
    
    * objet Patient 
    """
    daomaladie = DAOmaladie()

    try:
        proxies = {
            'http': 'http://pxcache-02.ensai.fr:3128',
            'https': 'http://pxcache-02.ensai.fr:3128'
        }

        # On envoie une requête pour récupérer les données sur les patients
        response = requests.get(
            'https://randomuser.me/api/?nat=us', proxies=proxies)

        # Une exception est envoyée si le status de la réponse indique une erreur
        response.raise_for_status()
        
        # On récupère la réponse au format json, et dans cette réponse le tableau de résultats (records)
        personne = response.json()['results']
        
        nom = personne[0]['name']['last']
        prenom = personne[0]['name']['first']
        titre = personne[0]['name']['title']

        budget_patient=random.randint(201,1000)
        frais_avocat=random.randint(100,200)

        mal = daomaladie.generate_one()
        id_mal=mal[0]
        jours_restants=mal[3]

        id_partie=id_partie
        id_salle = None
        id_patient=0
        

        patient=Patient(id_patient,id_mal,id_partie,id_salle,nom,prenom,titre,budget_patient,frais_avocat,jours_restants)
        
        return(patient)

    except requests.exceptions.RequestException:
        print("il y a eu une erreur")
        return( API_patient(id_partie) )
