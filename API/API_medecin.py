import requests
import random
from OBJET_METIER.medecin import Medecin
from DAO.dao_specialite import DAOspecialite

def API_medecins():
    """
    Cette fonction permet de récupérer 100 médecins grâce à une API (betterdoctor) \\

    output :

    * renvoie la liste des noms et la liste des prénoms des médecins (list of string)
    """
    
    try:

        proxies = {
            'http': 'http://pxcache-02.ensai.fr:3128',
            'https': 'http://pxcache-02.ensai.fr:3128'
        }

        # On envoie une requête pour récupérer les données sur les médecins

        response = requests.get(
            'https://api.betterdoctor.com/2016-03-01/doctors?location=37.773%2C-122.413%2C100&user_location=37.773%2C-122.413&skip=0&limit=100&user_key=3d5d0c278e39756e81bf8ef8e973da30', proxies=proxies)
        
        # Une exception est envoyée si le status de la réponse indique une erreur
        response.raise_for_status()
        
        # On récupère la réponse au format json, et dans cette réponse le tableau de résultats (records)
        
        medecin = response.json()['data']
        NOM=[]
        PRENOM=[]
        n=len(medecin)
        for i in range(n):
            NOM.append(medecin[i]['profile']['last_name'])
            PRENOM.append(medecin[i]['profile']['first_name'])

        return(NOM,PRENOM) # retourne 2 listes

    except requests.exceptions.RequestException :
        return(API_medecins())

