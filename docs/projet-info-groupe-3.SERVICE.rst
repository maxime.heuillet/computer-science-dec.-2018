projet-info-groupe-3.SERVICE package
====================================

Submodules
----------

projet-info-groupe-3.SERVICE.affichage_choix_journee module
-----------------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.affichage_choix_journee
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.SERVICE.affichage_patient module
-----------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.affichage_patient
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.SERVICE.lancer_partie module
-------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.lancer_partie
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.SERVICE.service_affichage module
-----------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.service_affichage
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.SERVICE.service_bilan module
-------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.service_bilan
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.SERVICE.service_medecin module
---------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.service_medecin
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.SERVICE.service_patient module
---------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.service_patient
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.SERVICE.supprimer_partie module
----------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.supprimer_partie
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.SERVICE.upgrade_couloir module
---------------------------------------------------

.. automodule:: projet-info-groupe-3.SERVICE.upgrade_couloir
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: projet-info-groupe-3.SERVICE
    :members:
    :undoc-members:
    :show-inheritance:
