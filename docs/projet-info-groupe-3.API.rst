projet-info-groupe-3.API package
================================

Submodules
----------

projet-info-groupe-3.API.API_medecin module
-------------------------------------------

.. automodule:: projet-info-groupe-3.API.API_medecin
    :members:
    :undoc-members:
    :show-inheritance:

projet-info-groupe-3.API.API_patient module
-------------------------------------------

.. automodule:: projet-info-groupe-3.API.API_patient
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: projet-info-groupe-3.API
    :members:
    :undoc-members:
    :show-inheritance:
