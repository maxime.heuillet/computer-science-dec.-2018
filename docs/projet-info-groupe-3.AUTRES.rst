projet-info-groupe-3.AUTRES package
===================================

Submodules
----------

projet-info-groupe-3.AUTRES.connection module
---------------------------------------------

.. automodule:: projet-info-groupe-3.AUTRES.connection
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: projet-info-groupe-3.AUTRES
    :members:
    :undoc-members:
    :show-inheritance:
